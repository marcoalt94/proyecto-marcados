@extends('layoutSimple')
@section('titulo', "Inicio")

@section('estilos-especiales')


    <link type="text/css" href="{{asset('css/style.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('css/reloj.css')}}" rel="stylesheet">
@endsection

@section('content')
<div class="container" style="min-height: 80vh;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <span class="display-4"> SISTEMA DE MARCADO </span>
            
            <div class="card mt-3">
                <div class="card-header">{{ __('Iniciar Sesión') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="ci" class="col-md-4 col-form-label text-md-right">{{ __('C. I.:') }}</label>

                            <div class="col-md-6">
                                <input id="ci" type="text" class="form-control{{ $errors->has('ci') ? ' is-invalid' : '' }}" name="ci" value="{{ old('ci') }}" required autofocus>

                                @if ($errors->has('ci'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ci') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password1" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña:') }}</label>

                            <div class="col-md-6">
                                <input id="password1" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary btn-lg">
                                    <i class="fas fa-fingerprint"></i> {{ __('ACCEDER') }} 
                                </button>

                                @if (Route::has('password.request'))
                                    <!-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('¿Olvidaste tu contraseña?') }}
                                    </a> -->
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>







        </div>
    </div>
    
    <div class="ml-5">
        <div class="wrap ml-5 pl-5">
            <div class="widget">
                <div class="fecha">
                    <p id="diaSemana" class="diaSemana"></p>
                    <p id="dia" class="dia"></p>
                    <p id="mes" class="mes"></p>
                    <p id="year" class="year"></p>
                </div>
                <div class="reloj">      
                    <p id="horas" class="horas"></p>
                    <p>:</p>
                    <p id="minutos" class="minutos"></p>
                    <p>:</p>

                    <div class="caja-segundos">
                        <p id="ampm" class="ampm"></p>
                        <p id="segundos" class="segundos"></p>
                    </div>
                    
                </div>
            </div>
        </div>

    </div>
        

    </div>
        

    
</div>
@endsection

@section('otroscript')

    <!-- <script src="{{asset('js/reloj.js')}}"></script> -->
@endsection
