<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('titulo') - MARCADOS</title>

    <link rel="shortcut icon" type="image/x-icon"  href="{{asset('img/favicon.ico')}}"/>
    
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    
    <link type="text/css" href="{{asset('css/fontawesome/iconos/all.css')}}" rel="stylesheet">

    <!-- <link href="https://fonts.googleapis.com/css?family=Livvic&display=swap" rel="stylesheet"> 
     -->
    @section('estilos-especiales')
        <link type="text/css" href="{{asset('css/style.css')}}" rel="stylesheet">
    @show
    <link href="{{asset('css/animate.min.css')}}" rel="stylesheet">

  </head>

  <body>
      <nav class="navbar navbar-expand-md navbar-dark bg-warning">
        <div class="container">
            <a class="navbar-brand " href="{{ url('/') }}">
                <img src="{{asset('img/logoM.png')}}" class="img-fluid" alt="*">
                - MARCADOS
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-principal" aria-controls="menu-principal" aria-expanded="false">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="menu-principal">

                <ul class="navbar-nav ml-auto px-4">
                  
                  @guest
                    <li class="nav-item">
                        <!-- <a class="nav-link" href="#"><i class="far fa-question-circle fa-lg"></i></a>
                         --><a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModalCenter">
                          <i class="far fa-question-circle fa-lg"></i>
                        </a>
                    </li>
                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                              <div class="modal-header bg-warning text-light">
                                <h5 class="modal-title " id="exampleModalCenterTitle">Información - Sistema de Marcado</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <p class="text-justify">
                                  Para registrar su ingreso o salida usted debe introducir su C.I. y su contraseña.<br>
                                  <hr>
                                  Los horarios normales de trabajo son de  <b>8:30 a 12:00</b>  y de  <b>14:30 a 19:00</b>. Se tiene una tolerancia de 
                                  10 min. para realizar el marcado, pasado este tiempo se registrará el atraso respectivo.<br>
                                  <ul>
                                  <li> Se considera como marcado de ingreso en la mañana hasta las 9:30</li>
                                  <li> Como salida de medio día se aceptan los marcados desde las 11:00 hasta las <b>13:59</b></li>
                                  <li> Como ingreso de la tarde se aceptan los marcados desde las <b>14:00</b> hasta las 15:30</li>
                                  <li> Como salida de la tarde se aceptan marcados desde las 18:00</li>
                                  </ul>
                                  Los días con horario continuo son de hrs. <b>8:30 a 16:30</b>. En estos casos se aceptan los marcados
                                  de medio día para la salida y el retorno.
                                </p>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                          </div>
                        </div>
                  @else
                      <li class="nav-item dropdown">
                          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                              
                              <?php $nom = explode(" ",Auth::user()->nombre); echo $nom[0]; ?>
                              <span class="caret"></span>
                          </a>

                          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                              <a class="dropdown-item" href="{{ route('usuarios.show', ['usuario' => Auth::user()]) }}">
                                  {{ __('Ver mi usuario') }}
                              </a>

                              
                                <a class="dropdown-item" href="{{route('usuarios.index')}}">Todos los usuarios</a>
                                <a class="dropdown-item" href="{{route('usuarios.create')}}">Nuevo usuario</a>
                                
                              
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="{{ route('logout') }}"
                                 onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                  {{ __('Cerrar sesión') }}
                              </a>

                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  @csrf
                              </form>

                          </div>
                      </li>
                  @endguest
                </ul>
            </div>
        </div>
      </nav>          
      @yield('portada')
    <div class="container">
      <!-- <div class="row"> -->

          @yield('content')

      <!-- </div> -->
    </div>

    <footer class="pt-4 bg-secondary">
      <div class="container">
        <div class="row">


          <div class="col-6">
            <span style="color: #ffffff;">Gobierno Autónomo Municipal de La Paz</span>
            <br>
            <span style="color: #ffffff;"> Calle Colón, Edif. Armando Escobar Uria Piso 6, Zona Centro</span><br>
            <span style="color: #ffffff;"> 2020</span>
            <br><br><br>
          </div>


        </div>
      </div>
    </footer>

    <script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/eventos.js')}}"></script>
    
    @yield('otroscript')

  </body>




</html>
