@extends('layoutSimple')


@section('titulo', "Editar Usuario")

@section('content')

	<h2 class="text-primary" id="tituloAnimado">Editar Usuario</h2>
	@if ($errors->any())
		<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
		</div>
		
	@endif

	<div class="container" style="min-height: 70vh;">
	    <div class="row justify-content-center">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-header">{{ __('Modifique datos deseados') }}</div>

	                <div class="card-body">

	                    <form method="POST" action="{{ route('usuarios.update', $usuario )}}">
	                        {{method_field('PUT')}}
							{{ csrf_field() }}


	                        <div class="form-group row">
	                            <label for="tipo" class="col-md-4 col-form-label text-md-right">{{ __('Tipo de usuario') }}</label>
	                            <div class="col-md-6">
	                                <select name="tipo" id="tipo">
							           	<option value="empleado" {{ old('tipo', $usuario->tipo) == 'empleado' ? 'selected' : '' }}>EMPLEADO</option>
							            <option value="administrador" {{ old('tipo', $usuario->tipo) == 'administrador' ? 'selected' : '' }}>ADMINISTRADOR</option>						           
							        </select>
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="ci" class="col-md-4 col-form-label text-md-right">{{ __('Nº Carnet de Identidad:') }}</label>
	                            <div class="col-md-6">
	                                <input id="ci" type="text" class="form-control{{ $errors->has('ci') ? ' is-invalid' : '' }}" name="ci" value="{{ old('ci', $usuario->ci) }}" required autofocus>
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="ap_paterno" class="col-md-4 col-form-label text-md-right">{{ __('Apellido Paterno:') }}</label>
	                            <div class="col-md-6">
	                                <input id="ap_paterno" type="text" class="form-control{{ $errors->has('ap_paterno') ? ' is-invalid' : '' }}" name="ap_paterno" value="{{ old('ap_paterno', $usuario->ap_paterno) }}" autofocus>
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="ap_materno" class="col-md-4 col-form-label text-md-right">{{ __('Apellido Materno:') }}</label>
	                            <div class="col-md-6">
	                                <input id="ap_materno" type="text" class="form-control{{ $errors->has('ap_materno') ? ' is-invalid' : '' }}" name="ap_materno" value="{{ old('ap_materno', $usuario->ap_materno) }}" autofocus>
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre:') }}</label>
	                            <div class="col-md-6">
	                                <input id="nombre" type="text" class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre" value="{{ old('nombre', $usuario->nombre) }}" required autofocus>
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo electronico:') }}</label>

	                            <div class="col-md-6">
	                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $usuario->email) }}" required>
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña:') }}</label>

	                            <div class="col-md-6">
	                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar contraseña:') }}</label>

	                            <div class="col-md-6">
	                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
	                            </div>
	                        </div>

	                        <div class="form-group row mb-0">
	                            <div class="col-md-6 offset-md-4">
	                                <button type="submit" class="btn btn-success">{{ __('Actualizar') }}</button>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	<br><br>
	<h5>
    <a href="{{ route('usuarios.show',['id'=> $usuario->id])}}"><i class="fas fa-arrow-circle-left"></i> Volver al Usuario</a></h5>
    <br><br>

	
@endsection