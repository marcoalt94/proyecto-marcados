@extends('layoutSimple')

@section('titulo', 'Crear usuario')

@section('content')

	<h2 class="text-primary">Crear Nuevo Usuario</h2>
	@if ($errors->any())
		<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
		</div>
		
	@endif


	<div class="container" style="min-height: 70vh;">
	    <div class="row justify-content-center">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-header">{{ __('Ingrese los datos del nuevo usuario:') }}</div>

	                <div class="card-body">

	                    <form method="POST" action="{{url('usuarios')}}">
	                        @csrf


	                        <div class="form-group row">
	                            <label for="tipo" class="col-md-4 col-form-label text-md-right">{{ __('Tipo de usuario:') }}</label>
	                            <div class="col-md-6">
	                                <select name="tipo" id="tipo">
							           	<option value="empleado">EMPELADO</option>
							           	<option value="administrador">ADMINISTRADOR</option>
							           			           
							        </select>
	                            </div>
	                        </div>


	                        <div class="form-group row">
	                            <label for="ci" class="col-md-4 col-form-label text-md-right">{{ __('Nº Carnet de Identidad:') }}</label>
	                            <div class="col-md-4">
	                                <input id="ci" type="text" class="form-control{{ $errors->has('ci') ? ' is-invalid' : '' }}" name="ci" value="{{ old('ci') }}" required autofocus>
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="ap_paterno" class="col-md-4 col-form-label text-md-right">{{ __('Apellido paterno:') }}</label>
	                            <div class="col-md-4">
	                                <input id="ap_paterno" type="text" class="form-control{{ $errors->has('ap_paterno') ? ' is-invalid' : '' }}" name="ap_paterno" value="{{ old('ap_paterno') }}" autofocus>
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="ap_materno" class="col-md-4 col-form-label text-md-right">{{ __('Apellido materno:') }}</label>
	                            <div class="col-md-4">
	                                <input id="ap_materno" type="text" class="form-control{{ $errors->has('ap_materno') ? ' is-invalid' : '' }}" name="ap_materno" value="{{ old('ap_materno') }}" autofocus>
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombres:') }}</label>
	                            <div class="col-md-4">
	                                <input id="nombre" type="text" class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre" value="{{ old('nombre') }}" required autofocus>
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo electrónico:') }}</label>

	                            <div class="col-md-4">
	                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña:') }}</label>

	                            <div class="col-md-4">
	                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
	                            	<span id="mensaje"></span>
	                                <div class="invalid-feedback" id="longitud">Debe contener al menos 8 caracteres</div>
	                                <div class="invalid-feedback" id="mayuscula">Debe contener al menos una mayúscula</div>
	                                <div class="invalid-feedback" id="minuscula">Debe contener al menos una minúscula</div>
	                                <div class="invalid-feedback" id="digito">Debe contener al menos un dígito</div>
	                                
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar contraseña:') }}</label>

	                            <div class="col-md-4">
	                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
	                            	<div class="invalid-feedback" id="confirm">Debe introducir la misma contraseña</div>
	                            </div>
	                        </div>

	                        <div class="form-group row mb-0">
	                            <div class="col-md-6 offset-md-4">
	                                <button type="submit" class="btn btn-primary btn-lg" id="botonAceptar">
	                                    <i class="fas fa-user-plus"></i>  {{ __('Crear usuario') }}
	                                </button>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	<br><br>
	<h5>
    <a href="{{ route('usuarios.index')}}"><i class="fas fa-arrow-circle-left"></i> Volver a Usuarios</a></h5>
    <br><br>

	
@endsection