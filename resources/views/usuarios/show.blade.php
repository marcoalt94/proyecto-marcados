@extends('layoutSimple')


@section('titulo', "Datos de usuario")


@section('content')

	@if(session('mensaje'))
		<div class="alert alert-success mt-3">
			<p>{{session('mensaje')}}</p>
		</div>
	@endif



	<div class="container" style="min-height: 70vh;">
	    <div class="row justify-content-center">
	        <div class="col-md-8 ">
				<h2 class="text-primary"><i class="far fa-address-card"></i> Detalles de {{$usuario->ci}}</h2>


	        	<table class="table mt-4 table-warning">
				  <tbody>
				  	<tr>
				      <th scope="row">C.I.:</th>
				      <td>{{$usuario->tipo}}</td>
				    </tr>
				  	<tr>
				      <th scope="row">C.I.:</th>
				      <td>{{$usuario->ci}}</td>
				    </tr>

				    <tr>
				      <th scope="row">Ap. Paterno:</th>
				      <td>{{$usuario->ap_paterno}}</td>
				    </tr>

				    <tr>
				      <th scope="row">Ap. Materno:</th>
				      <td>{{$usuario->ap_materno}}</td>
				    </tr>

				    <tr>
				      <th scope="row">Nombre:</th>
				      <td>{{$usuario->nombre}}</td>
				    </tr>

				    <tr>
				      <th scope="row">Correo electrónico:</th>
				      <td>{{$usuario->email}}</td>
				    </tr>

				  </tbody>
				</table>

				<a href="{{route('usuarios.edit', $usuario)}}" class="btn btn-primary"><i class="fas fa-user-edit"></i>Editar usuario</a>

	            	
	        </div>
	    </div>
	</div>

	<br><br>
	<h5>
    <a href="{{ route('marcados.index')}}"><i class="fas fa-arrow-circle-left"></i> Volver a página de inicio</a></h5>
    <br><br>

@endsection




