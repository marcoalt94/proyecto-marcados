@extends('layoutSimple')


@section('titulo', "Usuarios")

@section('content')


	<p> <a href="{{route('usuarios.create')}}" class="btn btn-success mt-3">Nuevo Usuario</a>
	</p>

	<div class="container" style="min-height: 75vh;">
		@if(session('mensaje'))
			<div class="alert alert-success">
				<p>{{session('mensaje')}}</p>
			</div>
		@endif
		@if(session('mensaje2'))
			<div class="alert alert-danger">
				<p>{{session('mensaje2')}}</p>
			</div>
		@endif

		

	    <div class="row ml-md-3">
	        <div class="col ">

				@if ($usuarios->isNotEmpty())

				<div class="form-group row" align="center">
					<div class="col-7"></div>
					<div class="col-3">
						<label for="mes" class="col-form-label text-md-right font-weight-bold">Mes:</label>
						<div align="right">
							<select name="mes" id="mes">
					        	<option value="01" {{ old('mes') == '01' ? 'selected' : '' }}>ENERO</option>
					        	<option value="02" {{ old('mes') == '02' ? 'selected' : '' }}>FEBRERO</option>
					        	<option value="03" {{ old('mes') == '03' ? 'selected' : '' }}>MARZO</option>
					        	<option value="04" {{ old('mes') == '04' ? 'selected' : '' }}>ABRIL</option>
					        	<option value="05" {{ old('mes') == '05' ? 'selected' : '' }}>MAYO</option>
					        	<option value="06" {{ old('mes') == '06' ? 'selected' : '' }}>JUNIO</option>
					        	<option value="07" {{ old('mes') == '07' ? 'selected' : '' }}>JULIO</option>
					        	<option value="08" {{ old('mes') == '08' ? 'selected' : '' }}>AGOSTO</option>
					        	<option value="09" {{ old('mes') == '09' ? 'selected' : '' }}>SEPTIEMBRE</option>
					        	<option value="10" {{ old('mes') == '10' ? 'selected' : '' }}>OCTUBRE</option>
					        	<option value="11" {{ old('mes') == '11' ? 'selected' : '' }}>NOVIEMBRE</option>
					        	<option value="12" {{ old('mes') == '12' ? 'selected' : '' }}>DICIEMBRE</option>
					        </select>
						</div>
					</div>
					<div class="col-2">
						<label for="gestion" class="col-form-label text-md-right font-weight-bold">Gestión:</label>
						<div align="right" >
							<select name="gestion" id="gestion">
					        	<option value="2020" {{ old('gestion') == '2020' ? 'selected' : '' }}>2020</option>
					        	<option value="2021" {{ old('gestion') == '2021' ? 'selected' : '' }}>2021</option>
					        	<option value="2022" {{ old('gestion') == '2022' ? 'selected' : '' }}>2022</option>
					        	<option value="2023" {{ old('gestion') == '2023' ? 'selected' : '' }}>2023</option>
					        	<option value="2024" {{ old('gestion') == '2024' ? 'selected' : '' }}>2024</option>
					        	<option value="2025" {{ old('gestion') == '2025' ? 'selected' : '' }}>2025</option>
					        </select>
						</div>
					</div>
				</div>

				<table class="table table-sm">
				  <thead class="thead">
				    <tr>
				      <th scope="col">#</th>
				      <th scope="col">Apellidos y nombres</th>
				      <th scope="col">C.I.</th>
				      <th scope="col">Ver</th>
				      <th scope="col">Editar</th>
				      <th scope="col">Borrar</th>
				      <th scope="col">Marcados</th>
				    </tr>
				  </thead>
				  <tbody>
				  	@foreach($usuarios as $usuario)
				    <tr>
					    <td scope="row">{{$loop->index + 1 }}</td>
					    <td>{{$usuario->ap_paterno}} {{$usuario->ap_materno}} {{$usuario->nombre}} </td>
					   	<td>{{$usuario->ci}}</td>
				    <td>
				      	<a href="{{route('usuarios.show',['id'=>$usuario])}}" class="btn btn-link"><i class="fas fa-eye"></i></a> 
				    </td>
				    <td><a href="{{route('usuarios.edit', $usuario)}}" class="btn btn-link"><i class="fas fa-user-edit"></i></a></td>
				    <td>
				    	<form action="{{route('usuarios.destroy',$usuario)}}" method="POST">
				      		{{csrf_field()}}
							{{method_field('DELETE')}}
							<button type="submit" class="btn btn-link" onclick="return confirmarEliminar()"><i class="fas fa-trash-alt"></i></button>
				      	</form>
				    	
					</td>
					<td><!-- <a href="{{route('usuarios.edit', $usuario)}}" class="btn btn-link">Ir a marcados</a></td> -->
						<button type="button" class="btn btn-link" onclick="marcados({{$usuario->ci}})">ver marcados</button>
				    </tr>
				    @endforeach
				   
				  </tbody>
				</table>
				@else
					<p>No hay usuarios registrados</p>
				@endif
			</div>
		</div>
	</div>

		<div class="d-flex justify-content-center">
			
		</div>

	<br><br>
    <a href="{{ route('marcados.index')}}"><i class="fas fa-arrow-circle-left"></i> Volver a página de inicio</a>
    <br><br>

@endsection

@section('otroscript')
	<script type="text/javascript">
		function confirmarEliminar(){
			var respuesta = confirm("¿Está seguro de eliminar al usuario del sistema de forma permanente?");
			return respuesta;
		}

		function marcados(ci){
			var mes = $('#mes').val();
			var gestion = $('#gestion').val();	
			var x= "{{asset('/')}}";
			document.location.href=(x+"marcados/detalle/"+ci+"/"+mes+"/"+gestion);
		}

	</script>
@endsection