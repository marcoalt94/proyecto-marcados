<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Reporte de asistencia</title>
  </head>
    <body>
      <div class="mx-3">
        <h3>REPORTE SEMANAL DE MARCADOS</h3>
        <p>Del: <b>{{$fecha_inicial}}</b>     Al: <b>{{$fecha_actual}}</b>
        </p>


        @foreach($arrayDatos as $datos)

          <p>- {{$datos['nom_usuario']}}</p>
          <table class="table" style="font-size:80%; margin-left: 35px;" border="1" cellspacing="0">
            <thead class="thead">
              <tr>
                <!-- <th scope="col">#</th> -->
                <th scope="col">Fecha</th>
                <th scope="col">Dia Semana</th>
                <th scope="col">Hora Ingreso</th>
                <th scope="col">Hora Salida</th>
                <th scope="col">Hora Ingreso</th>
                <th scope="col">Hora Salida</th>
                <th scope="col">Tipo</th>
                <th scope="col">Obs. Entr.</th>
                <th scope="col">Obs. Sal.</th>
              </tr>
            </thead>
            <tbody>
              @foreach($datos['arrayMes'] as $fecha)
              <tr>
                <!-- <td scope="row">{{$loop->index + 1 }}</td> -->
                <td>{{$fecha['dia']}}</td>
                <td>{{$fecha['nombre_dia']}}</td>
                  <td>
                      {{$fecha['ingreso_m']}}
                  </td>
                  <td>
                      {{$fecha['salida_m']}}
                  </td>
                  <td>

                      {{$fecha['ingreso_t']}}
                  </td>
                  <td>
                      {{$fecha['salida_t']}}
                  </td>
                  <td>
                    @if($fecha['horario_continuo'] == 'si')
                      CONTINUO
                    @endif
                  </td>
                  <td>
                    @if($fecha['obs_entrada'])
                      {{$fecha['obs_entrada']}}
                    @endif
                  </td>
                  <td>
                    @if($fecha['obs_salida'])
                      {{$fecha['obs_salida']}}
                    @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        @endforeach   

      </div>
    </body>
</html>
