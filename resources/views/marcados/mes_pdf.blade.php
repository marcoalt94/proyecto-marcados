<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Reporte de asistencia</title>
  </head>
    <body>
      <div class="mx-3">
        <h3>REPORTE DE MARCADOS</h3>
        <p>Nombre: {{$usuario->ap_paterno}} {{$usuario->ap_materno}} {{$usuario->nombre}}<br>
          Periodo: <b>{{$mes}} / {{$gestion}}</b>
        </p>
          <table class="table" style="font-size:80%;" border="1" cellspacing="0">
            <thead class="thead">
              <tr>
                <!-- <th scope="col">#</th> -->
                <th scope="col">Fecha</th>
                <th scope="col">Dia Semana</th>
                <th scope="col">Hora Ingreso</th>
                <th scope="col">Hora Salida</th>
                <th scope="col">Hora Ingreso</th>
                <th scope="col">Hora Salida</th>

                <th scope="col">Tipo</th>
                <th scope="col">Min. Atraso</th>
                <th scope="col">Obs. Entr.</th>
                <th scope="col">Obs. Sal.</th>
                <th scope="col">Min.Sal. Ant.</th>
              </tr>
            </thead>
            <tbody>
              @foreach($arrayMes as $fecha)
              <tr>
                <!-- <td scope="row">{{$loop->index + 1 }}</td> -->
                <td>{{$fecha['dia']}}</td>
                <td>{{$fecha['nombre_dia']}}</td>
                <td>
                      {{$fecha['ingreso_m']}}
                </td>
                <td>
                      {{$fecha['salida_m']}}
                </td>
                <td>
                      {{$fecha['ingreso_t']}}
                </td>
                <td>
                      {{$fecha['salida_t']}}
                </td>
                <td>
                    @if($fecha['horario_continuo'] == 'si')
                      CONTINUO
                    @endif
                </td>
                <td>
                    @if($fecha['min_atraso'] != 0)
                      {{$fecha['min_atraso']}}
                    @endif
                </td>

                <td>
                    @if($fecha['obs_entrada'])
                      {{$fecha['obs_entrada']}}
                    @endif
                </td>
                <td>
                    @if($fecha['obs_salida'])
                      {{$fecha['obs_salida']}}
                    @endif
                </td>

                <td>
                    @if($fecha['salida_ant'] != 0)
                    {{$fecha['salida_ant']}}
                    @endif
                </td>
              </tr>
              @endforeach
              <tr>
                <td class="bg-white" colspan="7"></td>
                <td >{{$atraso_total}}</td><td ></td><td ></td>
                <td >{{$sal_ant_total}}</td>
              </tr>
            </tbody>
          </table>
        </div>
       
    </body>
</html>
