@extends('layoutSimple')


@section('titulo', "Registro")


@section('content')

	@if(session('mensaje'))
		<div class="alert alert-danger">
			<p>{{session('mensaje')}}</p>
		</div>
	@endif



	<div class="container" style="min-height: 70vh;">

		<div class="row justify-content-center">
	        <div class="col-md-9">
	            <div class="card mt-3">
	                <div class="card-header">{{ __('REPORTE SEMANAL') }}
	                </div>
	                <div class="card-body">
	                  	Debe introducir la fecha del día <b>lunes</b> de la semana que quiere generar el reporte. El sistema generará los reportes de la semana
	                  	seleccionada de todos los usuarios de tipo empleado existentes en el sistema.<br><br>
	                  
	                    <form class="form" action="{{ route('marcados.semanal') }}" method="POST">
	                        @csrf
	                        <div class="form-group row">
	                            <label for="fecha_semana" class="col-md-4 col-form-label text-md-right">{{ __('Semana a buscar:') }}</label>
	                            <div class="col-md-6">
	                                <input id="fecha_semana" type="date" class="form-control{{ $errors->has('fecha_semana') ? ' is-invalid' : '' }}" name="fecha_semana" value="{{ old('fecha_semana') }}" required>
	                            </div>
	                        </div>
	                        <div class="form-group row mb-0">
	                            <div class="col-md-6 offset-md-4">
	                                <button type="submit" class="btn btn-success btn-lg" >
	                                    {{ __('BUSCAR') }}
	                                </button>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>




	    <div class="row justify-content-center my-3">
	        <div class="col-md-9">
	            <div class="card mt-3">
	                <div class="card-header">{{ __('REPORTE MENSUAL') }}</div>

	                <div class="card-body">
	                	En este caso, el sistema generará el reporte mensual del usuario con C.I. indicado en el formulario
	                	que se muestra a continuación.<br><br>

	                    <form class="form" action="#" method="GET">
	                        <div class="form-group row">
	                            <label for="ci" class="col-md-4 col-form-label text-md-right">{{ __('Nº Carnet de Identidad:') }}</label>
	                            <div class="col-md-6">
	                                <input id="ci" type="text" class="form-control{{ $errors->has('ci') ? ' is-invalid' : '' }}" name="ci" value="{{ old('ci') }}" required>
	                            </div>
	                        </div>


	                        <div class="form-group row">
								<label for="mes" class="col-md-4 col-form-label text-md-right font-weight-bold">Seleccione el mes:</label>
								<div class="col-md-3">
									<select name="mes" id="mes">
							        	<option value="01" {{ old('mes') == '01' ? 'selected' : '' }}>ENERO</option>
							        	<option value="02" {{ old('mes') == '02' ? 'selected' : '' }}>FEBRERO</option>
							        	<option value="03" {{ old('mes') == '03' ? 'selected' : '' }}>MARZO</option>
							        	<option value="04" {{ old('mes') == '04' ? 'selected' : '' }}>ABRIL</option>
							        	<option value="05" {{ old('mes') == '05' ? 'selected' : '' }}>MAYO</option>
							        	<option value="06" {{ old('mes') == '06' ? 'selected' : '' }}>JUNIO</option>
							        	<option value="07" {{ old('mes') == '07' ? 'selected' : '' }}>JULIO</option>
							        	<option value="08" {{ old('mes') == '08' ? 'selected' : '' }}>AGOSTO</option>
							        	<option value="09" {{ old('mes') == '09' ? 'selected' : '' }}>SEPTIEMBRE</option>
							        	<option value="10" {{ old('mes') == '10' ? 'selected' : '' }}>OCTUBRE</option>
							        	<option value="11" {{ old('mes') == '11' ? 'selected' : '' }}>NOVIEMBRE</option>
							        	<option value="12" {{ old('mes') == '12' ? 'selected' : '' }}>DICIEMBRE</option>
							        </select>
								</div>
							</div>


							<div class="form-group row">
								<label for="gestion" class="col-md-4 col-form-label text-md-right font-weight-bold">Seleccione la gestión:</label>
								<div class="col-md-3">
									<select name="gestion" id="gestion">
							        	<option value="2020" {{ old('gestion') == '2020' ? 'selected' : '' }}>2020</option>
							        	<option value="2021" {{ old('gestion') == '2021' ? 'selected' : '' }}>2021</option>
							        	<option value="2022" {{ old('gestion') == '2022' ? 'selected' : '' }}>2022</option>
							        	<option value="2023" {{ old('gestion') == '2023' ? 'selected' : '' }}>2023</option>
							        	<option value="2024" {{ old('gestion') == '2024' ? 'selected' : '' }}>2024</option>
							        	<option value="2025" {{ old('gestion') == '2025' ? 'selected' : '' }}>2025</option>
							        </select>
								</div>
							</div>




	                        <div class="form-group row mb-0">
	                            <div class="col-md-6 offset-md-4">
	                                <button type="button" class="btn btn-primary btn-lg" id="botonAceptar" onclick="marcados()">
	                                    {{ __('BUSCAR') }}
	                                </button>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

@endsection
@section('otroscript')
	<script type="text/javascript">
		function marcados(){
			var ci = $('#ci').val();
			var mes = $('#mes').val();
			var gestion = $('#gestion').val();	
			var x= "{{asset('/')}}";
			if(ci!=''){
				document.location.href=(x+"marcados/detalle/"+ci+"/"+mes+"/"+gestion);
			}
		}
	</script>
@endsection




