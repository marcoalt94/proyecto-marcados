@extends('layoutSimple')

@section('titulo', "Inicio")

@section('estilos-especiales')


    <link type="text/css" href="{{asset('css/style.css')}}" rel="stylesheet">

@endsection


@section('content')
	<!-- <div class="container" style="min-height: 70vh;">
		<br><br><br><br>
		<a href="{{ route('usuarios.index')}}" class="btn btn-lg btn-primary">GESTIONAR EMPLEADOS</a><br><br>
		<a href="{{ route('marcados.reportes')}}" class="btn btn-lg btn-info">REPORTES</a><br><br>
		<a href="{{ route('marcados.comision')}}">Asignar comisión</a>
	</div> -->
	<div class="container"style="min-height: 70vh;">
		<div class="card text-center mt-3">
			<div class="card-header">
				Administrador
			</div>
			<div class="card-body">
				<h4 class="card-title">SISTEMA DE MARCADO</h4>
				<p class="card-text">Elija una opción:</p>
				
				<div class="row" style="margin-right: 150px; margin-left: 150px;">
					<a href="{{ route('usuarios.index')}}" class="btn btn-lg btn-primary btn-block ">GESTIONAR EMPLEADOS</a><br><br>
				</div>
				<br>
				<div class="row" style="margin-right: 150px; margin-left: 150px;">
					<a href="{{ route('marcados.reportes')}}" class="btn btn-lg btn-info btn-block ">REPORTES</a><br><br>
				</div>
				<br>
				<div class="row" style="margin-right: 150px; margin-left: 150px;">
					<a href="{{ route('marcados.comision')}}" class="btn btn-lg btn-outline-success btn-block ">Asignar comisión</a>
				</div>
				<br>
				<div class="row" style="margin-right: 150px; margin-left: 150px;">
					<a href="{{ route('marcados.especial')}}" class="btn btn-lg btn-outline-secondary btn-block ">Día Especial</a>
				</div>

			</div>

			<div class="card-footer text-muted">
			<!-- 2 days ago -->
			</div>


		</div>
	
	</div>
	<br>
@endsection

@section('lateral-1')
	
    <div class="p-4 mb-2 bg-light rounded">

    	<a href="{{ route('usuarios.create')}}">
    		<i class="fas fa-plus"></i> crear usuario
		</a>
		

	</div>

@endsection


