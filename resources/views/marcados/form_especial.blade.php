@extends('layoutSimple')

@section('content')
<div class="container" style="min-height: 80vh;">
    @if ($errors->any())
    <br>
        <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        </div>
    @endif
    @if(session('mensaje'))
    <br>
        <div class="alert alert-success">
            <p>{{session('mensaje')}}</p>
        </div>
    @endif
    @if(session('mensaje2'))
    <br>
        <div class="alert alert-danger">
            <p>{{session('mensaje2')}}</p>
        </div>
    @endif


    <div class="row justify-content-center">
        <div class="col-md-8">
            <br>
            <h2>REGISTRAR DÍA ESPECIAL</h2><br><br>
            <form method="POST" action="{{ route('marcados.especial') }}">
                @csrf

                <div class="form-group row">
                    <label for="fecha" class="col-md-4 col-form-label text-md-right">Fecha:</label>
                    <div class="col-md-8">
                        <input type="date" name="fecha" id="fecha" value="{{ old('fecha') }}"> 
                    </div>
                </div>



                <div class="form-group row">
                    <label for="tipo" class="col-md-4 col-form-label text-md-right">{{ __('Tipo de asignación:') }}</label>
                    <div class="col-md-8">
                        <select name="tipo" id="tipo">
				           	<option value="continuo">HORARIO CONTINUO</option>
				           	<option value="continuo">Horario diferenciado</option>  
				        </select>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Registrar
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection
