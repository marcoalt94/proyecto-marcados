@extends('layoutSimple')


@section('titulo', "Registro")


@section('content')
	
	@if(session('mensaje'))
		<div class="alert alert-info">
			<p>{{session('mensaje')}}</p>
		</div>
	@endif
	@if(session('mensaje2'))
		<div class="alert alert-danger mt-2">
			<p>{{session('mensaje2')}}</p>
		</div>
	@endif

	<div class="container pt-2" style="min-height: 70vh;">
		@if($marcado!= null)
			<h2 class="text-primary">REGISTRO EXITOSO</h2>
		@else
			<h2 class="text-primary">SISTEMA DE REGISTRO</h2>
		@endif
		<br>
		<h3 class="text-center">{{Auth::user()->ap_paterno}} {{Auth::user()->ap_materno}} {{Auth::user()->nombre}}</h3>

	    <div class="row justify-content-center py-5">
	    		@if($marcado!= null)
				<h3> {{$marcado->hora}}</h3>
				@endif
	    </div>
	    <!-- <a href="{{ route('logout')}}"><i class="fas fa-arrow-circle-left"></i> SALIR</a></h5> -->

	   <div class="d-flex justify-content-center">



			<form id="logout-form" action="{{ route('logout') }}" method="POST" >
		    	@csrf
		    	<button type="submit" class="btn btn-lg btn-warning">CERRAR SESIÓN</button>
	  		</form>
		</div>
	    

	</div>


    

@endsection




