@extends('layoutSimple')

@section('titulo', "Usuarios")

@section('content')

	<h1 class="text-primary mt-1">REPORTE DE ASISTENCIA</h1>

	<div class="d-flex justify-content-between">
		<div class="p-2">

		Nombre: {{$usuario->ap_paterno}} {{$usuario->ap_materno}} {{$usuario->nombre}}<br>
		Periodo: <b>{{$mes}} / {{$gestion}}</b>
		</div>
	  	<div class="p-2">
	  		<a href="{{ route('marcados.mes_pdf',['ci' => $usuario->ci, 'mes' => $mes, 'gestion' => $gestion])}}" target="_blank" class="btn btn-warning">VER PDF</a>
	  	</div>
	</div>
	<div class="container" style="min-height: 75vh;">
		<table class="table table-sm table-bordered table-hover">
			<thead class="thead">
				<tr>
					<!-- <th scope="col">#</th> -->
					<th scope="col">Fecha</th>
					<th scope="col">Dia Semana</th>
					<th scope="col">Hora Ingreso</th>
					<th scope="col">Hora Salida</th>
					<th scope="col">Hora Ingreso</th>
					<th scope="col">Hora Salida</th>

					<th scope="col">Tipo</th>
					<th scope="col">Min. Atraso</th>
					<th scope="col">Obs. Entr.</th>
					<th scope="col">Obs. Sal.</th>
					<th scope="col">Min.Sal. Ant.</th>
				</tr>
			</thead>
			<tbody>
				@foreach($arrayMes as $fecha)
				<tr>
					<!-- <td scope="row">{{$loop->index + 1 }}</td> -->
					<td>{{$fecha['dia']}}</td>
					<td>{{$fecha['nombre_dia']}}</td>
				    <td>
				    	@if($fecha['ingreso_m'] == 'COMISION')
				    		<span class="text-primary">{{$fecha['ingreso_m']}}</span>
				    	@else
				    		{{$fecha['ingreso_m']}}
				    	@endif

					</td>
				    <td>
				    	@if($fecha['salida_m'] == 'COMISION')
				    		<span class="text-primary">{{$fecha['salida_m']}}</span>
				    	@else
				    		{{$fecha['salida_m']}}
				    	@endif
					</td>
				    <td>
				    	@if($fecha['ingreso_t'] == 'COMISION')
				    		<span class="text-primary">{{$fecha['ingreso_t']}}</span>
				    	@else
				    		{{$fecha['ingreso_t']}}
				    	@endif
				    </td>
				    <td>
				    	@if($fecha['salida_t'] == 'COMISION')
				    		<span class="text-primary">{{$fecha['salida_t']}}</span>
				    	@else
				    		{{$fecha['salida_t']}}
				    	@endif

				    </td>
				    <td class="text-danger">
				    	@if($fecha['horario_continuo'] == 'si')
				    		CONTINUO
				    	@endif
				    </td>
				    <td>
				    	@if($fecha['min_atraso'] != 0)
				    		{{$fecha['min_atraso']}}
				    	@endif
					</td>

				    <td>
				    	@if($fecha['obs_entrada'])
				    		{{$fecha['obs_entrada']}}
				    	@endif
				    </td>
				    <td>
				    	@if($fecha['obs_salida'])
				    		{{$fecha['obs_salida']}}
				    	@endif
					</td>

				    <td>
				    	@if($fecha['salida_ant'] != 0)
				    	{{$fecha['salida_ant']}}
				    	@endif
					</td>
				</tr>
				@endforeach
				<tr>
					<td class="bg-white" colspan="7"></td>
					<td class="bg-warning">{{$atraso_total}}</td><td class="bg-warning"></td><td class="bg-warning"></td>
					<td class="bg-warning">{{$sal_ant_total}}</td>
				</tr>

			</tbody>
		</table>		
	</div>
	<br><br>
    <a href="{{ route('marcados.index')}}"><i class="fas fa-arrow-circle-left"></i> Volver a página de inicio</a>
    <br><br>
@endsection
