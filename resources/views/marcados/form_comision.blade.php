@extends('layoutSimple')

@section('content')
<div class="container" style="min-height: 80vh;">
    @if ($errors->any())
    <br>
        <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        </div>
    @endif
    @if(session('mensaje'))
    <br>
        <div class="alert alert-success">
            <p>{{session('mensaje')}}</p>
        </div>
    @endif
    @if(session('mensaje2'))
    <br>
        <div class="alert alert-danger">
            <p>{{session('mensaje2')}}</p>
        </div>
    @endif


    <div class="row justify-content-center">
        <div class="col-md-8">
            
            <h2>ASIGNACIÓN DE COMISIÓN</h2><br><br>
            <form method="POST" action="{{ route('marcados.comision') }}">
                @csrf

                <div class="form-group row">
                    <label for="ci" class="col-md-4 col-form-label font-weight-bold">C.I. Usuario</label>
                    <div class="col-md-4">
                        <input id="ci" type="text" class="form-control{{ $errors->has('ci') ? ' is-invalid' : '' }}" name="ci" value="{{ old('ci') }}" required autofocus>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="fecha" class="col-md-4 col-form-label font-weight-bold">Fecha:</label>
                    <div class="col-md-8">
                        <input type="date" name="fecha" id="fecha" value="{{ old('fecha') }}"> 
                    </div>
                </div>

                <div class="form-group row">
                    <label for="periodos" class="col-md-4 col-form-label font-weight-bold">Periodo(s):</label><br>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-12">
                                <label>
                                    <input type="checkbox" name="periodos[]" value="ingreso_m" {{ (is_array(old('periodos')) and in_array('ingreso_m', old('periodos'))) ? ' checked' : '' }}>
                                        Ingreso mañana
                                </label>
                            </div>

                            <div class="col-12">
                                <label>
                                    <input type="checkbox" name="periodos[]" value="salida_m" {{ (is_array(old('periodos')) and in_array('salida_m', old('periodos'))) ? ' checked' : '' }}>
                                        Salida mañana
                                </label>
                            </div>

                            <div class="col-12">
                                <label>
                                    <input type="checkbox" name="periodos[]" value="ingreso_t" {{ (is_array(old('periodos')) and in_array('ingreso_t', old('periodos'))) ? ' checked' : '' }}>
                                        Ingreso tarde
                                </label>
                            </div>

                            <div class="col-12">
                                <label>
                                    <input type="checkbox" name="periodos[]" value="salida_t" {{ (is_array(old('periodos')) and in_array('salida_t', old('periodos'))) ? ' checked' : '' }}>
                                        Salida tarde
                                </label>
                            </div>
                        </div>
                    </div>
                </div>






                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Registrar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
