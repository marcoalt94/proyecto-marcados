@extends('layoutSimple')

@section('titulo', "Reporte semanal")

@section('content')

	<h1 class="text-primary mt-1">REPORTE SEMANAL DE MARCADOS</h1>

	<div class="d-flex justify-content-between">
		<div class="p-2">
		Del:<b>{{$fecha_inicial}}</b>     Al: <b>{{$fecha_actual}}</b>
		</div>
	  	<div class="p-2">
	  		<form class="form" target="_blank" action="{{ route('marcados.semanalpdf') }}" method="POST">
	            @csrf   
	  			<input id="fecha_semana" type="text" name="fecha_semana" style="display: none;">
	  			<button type="submit" class="btn btn-warning"> VER PDF </button>

	  		</form>
	  	</div>
	</div>
	<div class="container" style="min-height: 75vh;">


		@foreach($arrayDatos as $datos)

			<p><i class="fas fa-user"></i> {{$datos['nom_usuario']}}</p>

			<table class="table table-sm table-bordered table-hover">
				<thead class="thead">
					<tr>
						<!-- <th scope="col">#</th> -->
						<th scope="col">Fecha</th>
						<th scope="col">Dia Semana</th>
						<th scope="col">Hora Ingreso</th>
						<th scope="col">Hora Salida</th>
						<th scope="col">Hora Ingreso</th>
						<th scope="col">Hora Salida</th>
						<th scope="col">Tipo</th>
						<th scope="col">Obs. Entr.</th>
						<th scope="col">Obs. Sal.</th>
					</tr>
				</thead>
				<tbody>
					@foreach($datos['arrayMes'] as $fecha)
					<tr>
						<!-- <td scope="row">{{$loop->index + 1 }}</td> -->
						<td>{{$fecha['dia']}}</td>
						<td>{{$fecha['nombre_dia']}}</td>
					    <td>
					    	@if($fecha['ingreso_m'] == 'COMISION')
					    		<span class="text-primary">{{$fecha['ingreso_m']}}</span>
					    	@else
					    		{{$fecha['ingreso_m']}}
					    	@endif

						</td>
					    <td>
					    	@if($fecha['salida_m'] == 'COMISION')
					    		<span class="text-primary">{{$fecha['salida_m']}}</span>
					    	@else
					    		{{$fecha['salida_m']}}
					    	@endif
						</td>
					    <td>
					    	@if($fecha['ingreso_t'] == 'COMISION')
					    		<span class="text-primary">{{$fecha['ingreso_t']}}</span>
					    	@else
					    		{{$fecha['ingreso_t']}}
					    	@endif
					    </td>
					    <td>
					    	@if($fecha['salida_t'] == 'COMISION')
					    		<span class="text-primary">{{$fecha['salida_t']}}</span>
					    	@else
					    		{{$fecha['salida_t']}}
					    	@endif

					    </td>
					    <td class="text-danger">
					    	@if($fecha['horario_continuo'] == 'si')
					    		CONTINUO
					    	@endif
					    </td>


					    <td>
					    	@if($fecha['obs_entrada'])
					    		{{$fecha['obs_entrada']}}
					    	@endif
					    </td>
					    <td>
					    	@if($fecha['obs_salida'])
					    		{{$fecha['obs_salida']}}
					    	@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<br><br>

		@endforeach		
	</div>
	<br><br>
    <a href="{{ route('marcados.index')}}"><i class="fas fa-arrow-circle-left"></i> Volver a página de inicio</a>
    <br><br>
@endsection

@section('otroscript')
	<script type="text/javascript">
		var aux = {!! json_encode($fecha_inicial) !!};
		$('#fecha_semana').val(aux);
	</script>
@endsection
