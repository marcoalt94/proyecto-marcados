<?php

namespace App\Http\Controllers;
use App\Marcado;
use App\User;
use App\Dia_Especial;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MarcadoController extends Controller
{
    public function index(){

        $usuario = request()->user();
        if($usuario->tipo == 'administrador'){
            return view('marcados.index');
        }

        //EMPLEADOS

        $fecha_actual= date('Y-m-d');
        $hora_actual= date('H:i:s');
        
        $marcados = Marcado::where('usuario_id',$usuario->id)->where('fecha',$fecha_actual)->get();
        
        $time = Carbon::createFromFormat('H:i:s',$hora_actual);


        if(! Dia_Especial::where('fecha',$fecha_actual)->exists()){
            $time1 = Carbon::createFromFormat('H:i:s','09:30:00');
            $time2 = Carbon::createFromFormat('H:i:s','11:00:00');
            $time3 = Carbon::createFromFormat('H:i:s','13:59:59');
            $time4 = Carbon::createFromFormat('H:i:s','14:00:00');
            $time5 = Carbon::createFromFormat('H:i:s','15:30:00');
            $time6 = Carbon::createFromFormat('H:i:s','18:00:00');
            $time7 = Carbon::createFromFormat('H:i:s','23:59:00');

            $aux=0;
            if($time < $time1){
                foreach ($marcados as $marcado) {
                    $t_aux = Carbon::createFromFormat('H:i:s',$marcado->hora);
                    if($t_aux < $time1){
                        $aux++;

                        break;
                        
                    }
                }
            }

            if(($time > $time2) and ($time < $time3) ){
                foreach ($marcados as $marcado) {
                    $t_aux = Carbon::createFromFormat('H:i:s',$marcado->hora);
                    if(($t_aux > $time2) and ($t_aux < $time3)){
                        $aux++;

                        break;

                    }
                }
            }

            if(($time > $time4) and ($time < $time5) ){
                foreach ($marcados as $marcado) {
                    $t_aux = Carbon::createFromFormat('H:i:s',$marcado->hora);
                    if(($t_aux > $time4) and ($t_aux < $time5)){
                        $aux++;

                        break;

                    }
                }
            }
            
            if(($time > $time6) and ($time < $time7) ){
                foreach ($marcados as $marcado) {
                    $t_aux = Carbon::createFromFormat('H:i:s',$marcado->hora);
                    if(($t_aux > $time6) and ($t_aux < $time7)){
                        $aux++;
                        //dd('hhhhhhh');
                        break;

                    }

                }

                //dd('ssssss');
            }
           // dd('$aux');

            if($aux==0){
                $marcado= Marcado::create([
                    'usuario_id' => $usuario->id,
                    'fecha' => $fecha_actual,
                    'hora' => $hora_actual
                ]);
               return view('marcados.registrado',compact('marcado'));
            }else{
                $marcado=null;
                session()->flash('mensaje2','Usted ya se registró en el sistema.');
                return view('marcados.registrado',compact('marcado'));
            }

        }else{
            //HORARIO CONTINUO

            $time1 = Carbon::createFromFormat('H:i:s','09:30:00');

            $time6 = Carbon::createFromFormat('H:i:s','15:30:00');
            $time7 = Carbon::createFromFormat('H:i:s','23:59:00');


            $aux=0;
            if($time < $time1){
                foreach ($marcados as $marcado) {
                    $t_aux = Carbon::createFromFormat('H:i:s',$marcado->hora);
                    if($t_aux < $time1){
                        $aux++;
                        break;
                    }
                }
            }

            if(($time > $time1) and ($time < $time6)){
                $aux2=0;
                foreach ($marcados as $marcado) {
                    $t_aux = Carbon::createFromFormat('H:i:s',$marcado->hora);
                    if(($t_aux > $time1) and ($t_aux < $time6)){
                        $aux2++;
                        //verificar que no sea en ese instante
                        //sumamos 2 minutos a t_aux  y verificamos que time > esa suma
                        $tiempo = $t_aux->addMinutes(2);
                        if($time < $tiempo){
                            $aux++;
                            break;
                        }else{
                            if($aux2 == 2){
                                $aux++;
                            }
                        }
                    }
                }
            }

            
            if(($time > $time6) and ($time < $time7) ){
                foreach ($marcados as $marcado) {
                    $t_aux = Carbon::createFromFormat('H:i:s',$marcado->hora);
                    if(($t_aux > $time6) and ($t_aux < $time7)){
                        $aux++;
                        break;
                    }
                }
            }

            if($aux==0){
                $marcado= Marcado::create([
                    'usuario_id' => $usuario->id,
                    'fecha' => $fecha_actual,
                    'hora' => $hora_actual
                ]);
               return view('marcados.registrado',compact('marcado'));
            }else{
                $marcado=null;
                session()->flash('mensaje2','Usted ya se registró en el sistema.');
                return view('marcados.registrado',compact('marcado'));
            }


        }
    	return view('marcados.registrado',compact('marcado'));
    }


    //formulario
    public function reportes(){
        //dd('ssss');
        return view('marcados.reportes');
    }

    //detalle
    public function detalle($ci,$mes,$gestion){
        $datos=array(
            'ci' => $ci,
            'mes' => $mes,
            'gestion' => $gestion
        );
        $gestion = $datos['gestion'];
        $mes= $datos['mes'];

        if(User::where('ci',$datos['ci'])->exists()){
            $usuario = User::where('ci',$datos['ci'])->first();

            $fecha = $gestion.'-'.$mes.'-';

            //$marcados_mes = Marcado::where('usuario_id',$usuario->id)->where('fecha','LIKE',$fecha.'%')->get();

            //dd($marcados);
            $arrayMes = array();

            $ultimo_dia = Carbon::create($gestion, $mes, 1, 0, 0, 0)->endOfMonth();
            $cantidad_dias = $ultimo_dia->format('d');
            
            $dias_semana=['Monday'=>'Lunes','Tuesday'=>'Martes','Wednesday'=>'Miércoles','Thursday'=>'Jueves','Friday'=>'Viernes','Saturday'=>'Sábado','Sunday'=>'Domingo']; 
            $arrayMes = array();
            
            $atraso_total=0;
            $sal_ant_total=0;

            for ($i=1; $i <= $cantidad_dias ; $i++) { 
                $nom_dia = Carbon::createFromDate($gestion,$mes,$i)->format('l');

                //SABADOS Y DOMINGOS
                if($nom_dia == 'Saturday' or $nom_dia == 'Sunday'){
                    $arrayMes[] = array(
                        'dia' => $i.'/'.$mes.'/'.$gestion,
                        'nombre_dia' => $dias_semana[$nom_dia],
                        'ingreso_m' => '', 'salida_m' => '', 'ingreso_t' => '', 'salida_t' => '', 'min_atraso' => '', 'salida_ant' => '', 'obs_entrada' => '', 'obs_salida' => '','horario_continuo'=>''
                    );
                    continue;
                }

                $fecha_actual = $gestion.'-'.$mes.'-'.$i;
                $marcados_dia = Marcado::where('usuario_id',$usuario->id)->where('fecha',$fecha_actual)->get();
                $ingreso_m = '';$salida_m = '';$ingreso_t = '';$salida_t = '';
                

                $time1 = Carbon::createFromFormat('H:i:s','09:30:00');
                $time2 = Carbon::createFromFormat('H:i:s','11:00:00');
                $time3 = Carbon::createFromFormat('H:i:s','13:59:59');
                $time4 = Carbon::createFromFormat('H:i:s','14:00:00');
                $time5 = Carbon::createFromFormat('H:i:s','15:30:00');
                $time6 = Carbon::createFromFormat('H:i:s','18:00:00');
                $time7 = Carbon::createFromFormat('H:i:s','23:59:00');

                $hora1 = Carbon::createFromFormat('H:i:s','08:41:00');
                $hora2 = Carbon::createFromFormat('H:i:s','11:59:59');
                $hora3 = Carbon::createFromFormat('H:i:s','14:41:00');
                $hora4 = Carbon::createFromFormat('H:i:s','19:00:00');
                $sum_atraso = 0;
                $salida_ant = 0;
                $obs1=0;$obs2=0;$obs3=0;$obs4=0;
                

                //dd($time1->diffInMinutes($hora1));

                foreach ($marcados_dia as $m_dia) {   // 4 registros

                    $time = Carbon::createFromFormat('H:i:s',$m_dia->hora);
                    
                    if($time < $time1){
                        if($m_dia->comision == 'si'){
                            $ingreso_m = 'COMISION';
                        }else{
                            $ingreso_m = $m_dia->hora;

                            if($time >= $hora1){
                                $sum_atraso =  $hora1->diffInMinutes($time) + 11;
                            }
                        }
                        $obs1++;
                    }


                    if(($time > $time2) and ($time < $time3) ){
                        if($m_dia->comision == 'si'){
                            $salida_m = 'COMISION';
                        }else{
                            $salida_m = $m_dia->hora;

                            if($time <= $hora2 ){
                                $salida_ant = $hora2->diffInMinutes($time)+1;
                            }
                        }
                        $obs2++;
                    }


                    if(($time > $time4) and ($time < $time5) ){
                        if($m_dia->comision == 'si'){
                            $ingreso_t = 'COMISION';
                        }else{
                            $ingreso_t = $m_dia->hora;
                            if($time >= $hora3){
                                $sum_atraso = $sum_atraso + $hora3->diffInMinutes($time) + 11;
                            }
                        }
                        $obs3++;
                    }


                    if(($time > $time6) and ($time < $time7) ){
                        if($m_dia->comision == 'si'){
                            $salida_t = 'COMISION';
                        }else{
                            $salida_t = $m_dia->hora;
                            if($time < $hora4 ){
                                $salida_ant = $salida_ant + $hora4->diffInMinutes($time)+1;
                            }
                        }
                        $obs4++;
                    }
                }

                $obs_entrada = 0; $obs_salida = 0;
                if($obs1 == 0){
                    $ingreso_m = '---';
                    $obs_entrada++;
                }
                if($obs2 == 0){
                    $salida_m = '---';
                    $obs_salida++;
                }
                if($obs3 == 0){
                    $ingreso_t = '---';
                    $obs_entrada++;
                }
                if($obs4 == 0){
                    $salida_t = '---';
                    $obs_salida++;
                }


                if(! Dia_Especial::where('fecha',$fecha_actual)->exists()){
                    $time1 = Carbon::createFromFormat('H:i:s','09:30:00');
                    $time2 = Carbon::createFromFormat('H:i:s','11:00:00');
                    $time3 = Carbon::createFromFormat('H:i:s','13:59:59');
                    $time4 = Carbon::createFromFormat('H:i:s','14:00:00');
                    $time5 = Carbon::createFromFormat('H:i:s','15:30:00');
                    $time6 = Carbon::createFromFormat('H:i:s','18:00:00');
                    $time7 = Carbon::createFromFormat('H:i:s','23:59:00');

                    $hora1 = Carbon::createFromFormat('H:i:s','08:41:00');
                    $hora2 = Carbon::createFromFormat('H:i:s','11:59:59');
                    $hora3 = Carbon::createFromFormat('H:i:s','14:41:00');
                    $hora4 = Carbon::createFromFormat('H:i:s','19:00:00');
                    $sum_atraso = 0;
                    $salida_ant = 0;
                    $obs1=0;$obs2=0;$obs3=0;$obs4=0;
                    

                    //dd($time1->diffInMinutes($hora1));

                    foreach ($marcados_dia as $m_dia) {   // 4 registros

                        $time = Carbon::createFromFormat('H:i:s',$m_dia->hora);
                        
                        if($time < $time1){
                            if($m_dia->comision == 'si'){
                                $ingreso_m = 'COMISION';
                            }else{
                                $ingreso_m = $m_dia->hora;

                                if($time >= $hora1){
                                    $sum_atraso =  $hora1->diffInMinutes($time) + 11;
                                }
                            }
                            $obs1++;
                        }


                        if(($time > $time2) and ($time < $time3) ){
                            if($m_dia->comision == 'si'){
                                $salida_m = 'COMISION';
                            }else{
                                $salida_m = $m_dia->hora;

                                if($time <= $hora2 ){
                                    $salida_ant = $hora2->diffInMinutes($time)+1;
                                }
                            }
                            $obs2++;
                        }


                        if(($time > $time4) and ($time < $time5) ){
                            if($m_dia->comision == 'si'){
                                $ingreso_t = 'COMISION';
                            }else{
                                $ingreso_t = $m_dia->hora;
                                if($time >= $hora3){
                                    $sum_atraso = $sum_atraso + $hora3->diffInMinutes($time) + 11;
                                }
                            }
                            $obs3++;
                        }


                        if(($time > $time6) and ($time < $time7) ){
                            if($m_dia->comision == 'si'){
                                $salida_t = 'COMISION';
                            }else{
                                $salida_t = $m_dia->hora;
                                if($time < $hora4 ){
                                    $salida_ant = $salida_ant + $hora4->diffInMinutes($time)+1;
                                }
                            }
                            $obs4++;
                        }
                    }

                    $obs_entrada = 0; $obs_salida = 0;
                    if($obs1 == 0){
                        $ingreso_m = '---';
                        $obs_entrada++;
                    }
                    if($obs2 == 0){
                        $salida_m = '---';
                        $obs_salida++;
                    }
                    if($obs3 == 0){
                        $ingreso_t = '---';
                        $obs_entrada++;
                    }
                    if($obs4 == 0){
                        $salida_t = '---';
                        $obs_salida++;
                    }
                    $continuo = 'no';
                }else{
                    /////////////HORARIO CONTINUO


                    $time1 = Carbon::createFromFormat('H:i:s','09:30:00');


                    $time2 = Carbon::createFromFormat('H:i:s','11:00:00');
                    //$time3 = Carbon::createFromFormat('H:i:s','13:59:59');
                    //$time4 = Carbon::createFromFormat('H:i:s','14:00:00');
                    $time5 = Carbon::createFromFormat('H:i:s','14:00:00');


                    $time6 = Carbon::createFromFormat('H:i:s','16:00:00');
                    $time7 = Carbon::createFromFormat('H:i:s','23:59:00');



                    $hora1 = Carbon::createFromFormat('H:i:s','08:41:00');
                    //$hora2 = Carbon::createFromFormat('H:i:s','11:59:59');
                    //$hora3 = Carbon::createFromFormat('H:i:s','14:41:00');
                    $hora4 = Carbon::createFromFormat('H:i:s','16:30:00');
                    $sum_atraso = 0;
                    $salida_ant = 0;
                    $obs1=0;$obs2=0;$obs3=0;$obs4=0;

                    $c=0;
                    foreach ($marcados_dia as $m_dia) {   // 4 registros

                        $time = Carbon::createFromFormat('H:i:s',$m_dia->hora);
                        
                        if($time < $time1){
                            if($m_dia->comision == 'si'){
                                $ingreso_m = 'COMISION';
                            }else{
                                $ingreso_m = $m_dia->hora;

                                if($time >= $hora1){
                                    $sum_atraso =  $hora1->diffInMinutes($time) + 11;
                                }
                            }
                            $obs1++;
                        }


                        if(($time > $time2) and ($time < $time5) ){
                            
                            if($c==0){
                                $c++;
                                if($m_dia->comision == 'si'){
                                    $salida_m = 'COMISION';
                                }else{
                                    $salida_m = $m_dia->hora;
                                }
                                $obs2++;
                            }else{
                                if($m_dia->comision == 'si'){
                                    $ingreso_t = 'COMISION';
                                }else{
                                    $ingreso_t = $m_dia->hora;

                                }
                                $obs3++;
                            }
                        }


                        if(($time > $time6) and ($time < $time7) ){
                            if($m_dia->comision == 'si'){
                                $salida_t = 'COMISION';
                            }else{
                                $salida_t = $m_dia->hora;
                                if($time < $hora4 ){
                                    $salida_ant = $salida_ant + $hora4->diffInMinutes($time)+1;
                                }
                            }
                            $obs4++;
                        }
                    }

                    $obs_entrada = 0; $obs_salida = 0;
                    if($obs1 == 0){
                        $ingreso_m = '---';
                        $obs_entrada++;
                    }
                    if($obs2 == 0){
                        $salida_m = '---';
                        $obs_salida++;
                    }
                    if($obs3 == 0){
                        $ingreso_t = '---';
                        $obs_entrada++;
                    }
                    if($obs4 == 0){
                        $salida_t = '---';
                        $obs_salida++;
                    }
                    $continuo = 'si';
                }


                $arrayMes[] = array(
                    'dia' => $i.'/'.$mes.'/'.$gestion,
                    'nombre_dia' => $dias_semana[$nom_dia],
                    'ingreso_m' => $ingreso_m,
                    'salida_m' => $salida_m,
                    'ingreso_t' => $ingreso_t,
                    'salida_t' => $salida_t,
                    'min_atraso' => $sum_atraso,
                    'salida_ant' => $salida_ant,
                    'obs_entrada' => $obs_entrada,
                    'obs_salida' => $obs_salida,

                    'horario_continuo' => $continuo,
                );
                $atraso_total = $atraso_total + $sum_atraso;
                $sal_ant_total = $sal_ant_total+$salida_ant;

                //$gestion_ = $mes.' / '.$gestion;

            }
            
            return view('marcados.detalle', compact('usuario','arrayMes','atraso_total','sal_ant_total','gestion','mes'));

        }
        session()->flash('mensaje','Usuario no encontrado. Verifique e intente nuevamente.');
        return redirect()->route('marcados.reportes');
    }


    public function comision(){
        return view('marcados.form_comision');
    }



    public function asignarComision(){
        $datos=request() ->validate([
            'ci' => 'required',
            'fecha' => 'required',
            'periodos'=> 'required',
        ]);
        if(User::where('ci',$datos['ci'])->exists()){
            $usuario = User::where('ci',$datos['ci'])->first();


            $marcados = Marcado::where('usuario_id',$usuario->id)->where('fecha',$datos['fecha'])->get();


            if(! Dia_Especial::where('fecha',$datos['fecha'])->exists()){
                $time1 = Carbon::createFromFormat('H:i:s','09:30:00');
                $time2 = Carbon::createFromFormat('H:i:s','11:00:00');
                $time3 = Carbon::createFromFormat('H:i:s','13:59:59');
                $time4 = Carbon::createFromFormat('H:i:s','14:00:00');
                $time5 = Carbon::createFromFormat('H:i:s','15:30:00');
                $time6 = Carbon::createFromFormat('H:i:s','18:00:00');
                $time7 = Carbon::createFromFormat('H:i:s','23:59:00');

                foreach ($datos['periodos'] as $periodo) {
                    if($periodo == 'ingreso_m'){
                        foreach ($marcados as $marcado) {
                            $time = Carbon::createFromFormat('H:i:s',$marcado->hora);
                            if($time < $time1){
                                $marcado->delete();
                            }
                        }
                        $marcado= Marcado::create([
                            'usuario_id' => $usuario->id,
                            'fecha' => $datos['fecha'],
                            'hora' => '08:30:00',
                            'comision' => 'si'
                        ]);                    
                    }
                    if($periodo == 'salida_m'){
                        foreach ($marcados as $marcado) {
                            $time = Carbon::createFromFormat('H:i:s',$marcado->hora);
                            if(($time > $time2) and ($time < $time3) ){
                                $marcado->delete();
                            }
                        }
                        $marcado= Marcado::create([
                            'usuario_id' => $usuario->id,
                            'fecha' => $datos['fecha'],
                            'hora' => '12:00:01',
                            'comision' => 'si'
                        ]);                    
                    }
                    if($periodo == 'ingreso_t'){
                        foreach ($marcados as $marcado) {
                            $time = Carbon::createFromFormat('H:i:s',$marcado->hora);
                            if(($time > $time4) and ($time < $time5) ){
                                $marcado->delete();
                            }
                        }
                        $marcado= Marcado::create([
                            'usuario_id' => $usuario->id,
                            'fecha' => $datos['fecha'],
                            'hora' => '14:30:00',
                            'comision' => 'si'
                        ]);                    
                    }
                    if($periodo == 'salida_t'){
                        foreach ($marcados as $marcado) {
                            $time = Carbon::createFromFormat('H:i:s',$marcado->hora);
                            if(($time > $time6) and ($time < $time7) ){
                                $marcado->delete();
                            }
                        }
                        $marcado= Marcado::create([
                            'usuario_id' => $usuario->id,
                            'fecha' => $datos['fecha'],
                            'hora' => '19:00:01',
                            'comision' => 'si'
                        ]);                    
                    }

                }

            }else{          ////////////horario continuo
                $time1 = Carbon::createFromFormat('H:i:s','09:30:00');
                /*$time2 = Carbon::createFromFormat('H:i:s','11:00:00');
                $time3 = Carbon::createFromFormat('H:i:s','13:59:59');
                $time4 = Carbon::createFromFormat('H:i:s','14:00:00');
                $time5 = Carbon::createFromFormat('H:i:s','15:30:00');*/
                $time6 = Carbon::createFromFormat('H:i:s','18:00:00');
                $time7 = Carbon::createFromFormat('H:i:s','23:59:00');

                foreach ($datos['periodos'] as $periodo) {
                    if($periodo == 'ingreso_m'){
                        foreach ($marcados as $marcado) {
                            $time = Carbon::createFromFormat('H:i:s',$marcado->hora);
                            if($time < $time1){
                                $marcado->delete();
                            }
                        }
                        $marcado= Marcado::create([
                            'usuario_id' => $usuario->id,
                            'fecha' => $datos['fecha'],
                            'hora' => '08:30:00',
                            'comision' => 'si'
                        ]);                    
                    }


                    if($periodo == 'salida_m'){
                        $marcado= Marcado::create([
                            'usuario_id' => $usuario->id,
                            'fecha' => $datos['fecha'],
                            'hora' => '12:00:01',
                            'comision' => 'si'
                        ]);                    
                    }


                    if($periodo == 'ingreso_t'){
                        $marcado= Marcado::create([
                            'usuario_id' => $usuario->id,
                            'fecha' => $datos['fecha'],
                            'hora' => '13:31:00',
                            'comision' => 'si'
                        ]);                    
                    }

                    if($periodo == 'salida_t'){
                        foreach ($marcados as $marcado) {
                            $time = Carbon::createFromFormat('H:i:s',$marcado->hora);
                            if(($time > $time6) and ($time < $time7) ){
                                $marcado->delete();
                            }
                        }
                        $marcado= Marcado::create([
                            'usuario_id' => $usuario->id,
                            'fecha' => $datos['fecha'],
                            'hora' => '19:01:00',
                            'comision' => 'si'
                        ]);                    
                    }

                }

            }


            
            session()->flash('mensaje','Asignación exitosa.');
            return redirect()->route('marcados.comision');
        }else{
            session()->flash('mensaje2','Usuario no encontrado. Verifique e intente nuevamente.');
            return redirect()->route('marcados.comision');
        }

    }


    public function dia_especial(){
        return view('marcados.form_especial');
    }

    public function registro_especial(){
        $datos=request() ->validate([
            'fecha' => 'required',
            'tipo'=> 'required',
        ]);
        $especial= Dia_Especial::create([
            'fecha' => $datos['fecha'],
            'tipo' => $datos['tipo']
        ]);
        //dd($especial);
        session()->flash('mensaje','Registro exitoso.');
        return redirect()->route('marcados.especial');
    }

    public function detallepdf($ci, $mes, $gestion){

        $datos=array(
            'ci' => $ci,
            'mes' => $mes,
            'gestion' => $gestion
        );
        $gestion = $datos['gestion'];
        $mes= $datos['mes'];

        if(User::where('ci',$datos['ci'])->exists()){
            $usuario = User::where('ci',$datos['ci'])->first();

            $fecha = $gestion.'-'.$mes.'-';

            //$marcados_mes = Marcado::where('usuario_id',$usuario->id)->where('fecha','LIKE',$fecha.'%')->get();

            //dd($marcados);
            $arrayMes = array();

            $ultimo_dia = Carbon::create($gestion, $mes, 1, 0, 0, 0)->endOfMonth();
            $cantidad_dias = $ultimo_dia->format('d');
            
            $dias_semana=['Monday'=>'Lunes','Tuesday'=>'Martes','Wednesday'=>'Miércoles','Thursday'=>'Jueves','Friday'=>'Viernes','Saturday'=>'Sábado','Sunday'=>'Domingo']; 
            $arrayMes = array();
            
            $atraso_total=0;
            $sal_ant_total=0;

            for ($i=1; $i <= $cantidad_dias ; $i++) { 
                $nom_dia = Carbon::createFromDate($gestion,$mes,$i)->format('l');

                //SABADOS Y DOMINGOS
                if($nom_dia == 'Saturday' or $nom_dia == 'Sunday'){
                    $arrayMes[] = array(
                        'dia' => $i.'/'.$mes.'/'.$gestion,
                        'nombre_dia' => $dias_semana[$nom_dia],
                        'ingreso_m' => '', 'salida_m' => '', 'ingreso_t' => '', 'salida_t' => '', 'min_atraso' => '', 'salida_ant' => '', 'obs_entrada' => '', 'obs_salida' => '','horario_continuo'=>''
                    );
                    continue;
                }

                $fecha_actual = $gestion.'-'.$mes.'-'.$i;
                $marcados_dia = Marcado::where('usuario_id',$usuario->id)->where('fecha',$fecha_actual)->get();
                $ingreso_m = '';$salida_m = '';$ingreso_t = '';$salida_t = '';
                

                $time1 = Carbon::createFromFormat('H:i:s','09:30:00');
                $time2 = Carbon::createFromFormat('H:i:s','11:00:00');
                $time3 = Carbon::createFromFormat('H:i:s','13:59:59');
                $time4 = Carbon::createFromFormat('H:i:s','14:00:00');
                $time5 = Carbon::createFromFormat('H:i:s','15:30:00');
                $time6 = Carbon::createFromFormat('H:i:s','18:00:00');
                $time7 = Carbon::createFromFormat('H:i:s','23:59:00');

                $hora1 = Carbon::createFromFormat('H:i:s','08:41:00');
                $hora2 = Carbon::createFromFormat('H:i:s','11:59:59');
                $hora3 = Carbon::createFromFormat('H:i:s','14:41:00');
                $hora4 = Carbon::createFromFormat('H:i:s','19:00:00');
                $sum_atraso = 0;
                $salida_ant = 0;
                $obs1=0;$obs2=0;$obs3=0;$obs4=0;
                

                //dd($time1->diffInMinutes($hora1));

                foreach ($marcados_dia as $m_dia) {   // 4 registros

                    $time = Carbon::createFromFormat('H:i:s',$m_dia->hora);
                    
                    if($time < $time1){
                        if($m_dia->comision == 'si'){
                            $ingreso_m = 'COMISION';
                        }else{
                            $ingreso_m = $m_dia->hora;

                            if($time >= $hora1){
                                $sum_atraso =  $hora1->diffInMinutes($time) + 11;
                            }
                        }
                        $obs1++;
                    }


                    if(($time > $time2) and ($time < $time3) ){
                        if($m_dia->comision == 'si'){
                            $salida_m = 'COMISION';
                        }else{
                            $salida_m = $m_dia->hora;

                            if($time <= $hora2 ){
                                $salida_ant = $hora2->diffInMinutes($time)+1;
                            }
                        }
                        $obs2++;
                    }


                    if(($time > $time4) and ($time < $time5) ){
                        if($m_dia->comision == 'si'){
                            $ingreso_t = 'COMISION';
                        }else{
                            $ingreso_t = $m_dia->hora;
                            if($time >= $hora3){
                                $sum_atraso = $sum_atraso + $hora3->diffInMinutes($time) + 11;
                            }
                        }
                        $obs3++;
                    }


                    if(($time > $time6) and ($time < $time7) ){
                        if($m_dia->comision == 'si'){
                            $salida_t = 'COMISION';
                        }else{
                            $salida_t = $m_dia->hora;
                            if($time < $hora4 ){
                                $salida_ant = $salida_ant + $hora4->diffInMinutes($time)+1;
                            }
                        }
                        $obs4++;
                    }
                }

                $obs_entrada = 0; $obs_salida = 0;
                if($obs1 == 0){
                    $ingreso_m = '---';
                    $obs_entrada++;
                }
                if($obs2 == 0){
                    $salida_m = '---';
                    $obs_salida++;
                }
                if($obs3 == 0){
                    $ingreso_t = '---';
                    $obs_entrada++;
                }
                if($obs4 == 0){
                    $salida_t = '---';
                    $obs_salida++;
                }


                if(! Dia_Especial::where('fecha',$fecha_actual)->exists()){
                    $time1 = Carbon::createFromFormat('H:i:s','09:30:00');
                    $time2 = Carbon::createFromFormat('H:i:s','11:00:00');
                    $time3 = Carbon::createFromFormat('H:i:s','13:59:59');
                    $time4 = Carbon::createFromFormat('H:i:s','14:00:00');
                    $time5 = Carbon::createFromFormat('H:i:s','15:30:00');
                    $time6 = Carbon::createFromFormat('H:i:s','18:00:00');
                    $time7 = Carbon::createFromFormat('H:i:s','23:59:00');

                    $hora1 = Carbon::createFromFormat('H:i:s','08:41:00');
                    $hora2 = Carbon::createFromFormat('H:i:s','11:59:59');
                    $hora3 = Carbon::createFromFormat('H:i:s','14:41:00');
                    $hora4 = Carbon::createFromFormat('H:i:s','19:00:00');
                    $sum_atraso = 0;
                    $salida_ant = 0;
                    $obs1=0;$obs2=0;$obs3=0;$obs4=0;
                    

                    //dd($time1->diffInMinutes($hora1));

                    foreach ($marcados_dia as $m_dia) {   // 4 registros

                        $time = Carbon::createFromFormat('H:i:s',$m_dia->hora);
                        
                        if($time < $time1){
                            if($m_dia->comision == 'si'){
                                $ingreso_m = 'COMISION';
                            }else{
                                $ingreso_m = $m_dia->hora;

                                if($time >= $hora1){
                                    $sum_atraso =  $hora1->diffInMinutes($time) + 11;
                                }
                            }
                            $obs1++;
                        }


                        if(($time > $time2) and ($time < $time3) ){
                            if($m_dia->comision == 'si'){
                                $salida_m = 'COMISION';
                            }else{
                                $salida_m = $m_dia->hora;

                                if($time <= $hora2 ){
                                    $salida_ant = $hora2->diffInMinutes($time)+1;
                                }
                            }
                            $obs2++;
                        }


                        if(($time > $time4) and ($time < $time5) ){
                            if($m_dia->comision == 'si'){
                                $ingreso_t = 'COMISION';
                            }else{
                                $ingreso_t = $m_dia->hora;
                                if($time >= $hora3){
                                    $sum_atraso = $sum_atraso + $hora3->diffInMinutes($time) + 11;
                                }
                            }
                            $obs3++;
                        }


                        if(($time > $time6) and ($time < $time7) ){
                            if($m_dia->comision == 'si'){
                                $salida_t = 'COMISION';
                            }else{
                                $salida_t = $m_dia->hora;
                                if($time < $hora4 ){
                                    $salida_ant = $salida_ant + $hora4->diffInMinutes($time)+1;
                                }
                            }
                            $obs4++;
                        }
                    }

                    $obs_entrada = 0; $obs_salida = 0;
                    if($obs1 == 0){
                        $ingreso_m = '---';
                        $obs_entrada++;
                    }
                    if($obs2 == 0){
                        $salida_m = '---';
                        $obs_salida++;
                    }
                    if($obs3 == 0){
                        $ingreso_t = '---';
                        $obs_entrada++;
                    }
                    if($obs4 == 0){
                        $salida_t = '---';
                        $obs_salida++;
                    }
                    $continuo = 'no';
                }else{
                    /////////////HORARIO CONTINUO


                    $time1 = Carbon::createFromFormat('H:i:s','09:30:00');


                    $time2 = Carbon::createFromFormat('H:i:s','11:00:00');
                    //$time3 = Carbon::createFromFormat('H:i:s','13:59:59');
                    //$time4 = Carbon::createFromFormat('H:i:s','14:00:00');
                    $time5 = Carbon::createFromFormat('H:i:s','14:00:00');


                    $time6 = Carbon::createFromFormat('H:i:s','16:00:00');
                    $time7 = Carbon::createFromFormat('H:i:s','23:59:00');



                    $hora1 = Carbon::createFromFormat('H:i:s','08:41:00');
                    //$hora2 = Carbon::createFromFormat('H:i:s','11:59:59');
                    //$hora3 = Carbon::createFromFormat('H:i:s','14:41:00');
                    $hora4 = Carbon::createFromFormat('H:i:s','16:30:00');
                    $sum_atraso = 0;
                    $salida_ant = 0;
                    $obs1=0;$obs2=0;$obs3=0;$obs4=0;

                    $c=0;
                    foreach ($marcados_dia as $m_dia) {   // 4 registros

                        $time = Carbon::createFromFormat('H:i:s',$m_dia->hora);
                        
                        if($time < $time1){
                            if($m_dia->comision == 'si'){
                                $ingreso_m = 'COMISION';
                            }else{
                                $ingreso_m = $m_dia->hora;

                                if($time >= $hora1){
                                    $sum_atraso =  $hora1->diffInMinutes($time) + 11;
                                }
                            }
                            $obs1++;
                        }


                        if(($time > $time2) and ($time < $time5) ){
                            
                            if($c==0){
                                $c++;
                                if($m_dia->comision == 'si'){
                                    $salida_m = 'COMISION';
                                }else{
                                    $salida_m = $m_dia->hora;
                                }
                                $obs2++;
                            }else{
                                if($m_dia->comision == 'si'){
                                    $ingreso_t = 'COMISION';
                                }else{
                                    $ingreso_t = $m_dia->hora;

                                }
                                $obs3++;
                            }
                        }


                        if(($time > $time6) and ($time < $time7) ){
                            if($m_dia->comision == 'si'){
                                $salida_t = 'COMISION';
                            }else{
                                $salida_t = $m_dia->hora;
                                if($time < $hora4 ){
                                    $salida_ant = $salida_ant + $hora4->diffInMinutes($time)+1;
                                }
                            }
                            $obs4++;
                        }
                    }

                    $obs_entrada = 0; $obs_salida = 0;
                    if($obs1 == 0){
                        $ingreso_m = '---';
                        $obs_entrada++;
                    }
                    if($obs2 == 0){
                        $salida_m = '---';
                        $obs_salida++;
                    }
                    if($obs3 == 0){
                        $ingreso_t = '---';
                        $obs_entrada++;
                    }
                    if($obs4 == 0){
                        $salida_t = '---';
                        $obs_salida++;
                    }
                    $continuo = 'si';
                }


                $arrayMes[] = array(
                    'dia' => $i.'/'.$mes.'/'.$gestion,
                    'nombre_dia' => $dias_semana[$nom_dia],
                    'ingreso_m' => $ingreso_m,
                    'salida_m' => $salida_m,
                    'ingreso_t' => $ingreso_t,
                    'salida_t' => $salida_t,
                    'min_atraso' => $sum_atraso,
                    'salida_ant' => $salida_ant,
                    'obs_entrada' => $obs_entrada,
                    'obs_salida' => $obs_salida,

                    'horario_continuo' => $continuo,
                );
                $atraso_total = $atraso_total + $sum_atraso;
                $sal_ant_total = $sal_ant_total+$salida_ant;

                //$gestion_ = $mes.' / '.$gestion;
            }
            
            //return view('marcados.detalle', compact('usuario','arrayMes','atraso_total','sal_ant_total','gestion','mes'));

            $pdf = \PDF::loadview('marcados.mes_pdf',[
                'usuario'=>$usuario,
                'arrayMes' => $arrayMes,
                'atraso_total' => $atraso_total,
                'sal_ant_total' => $sal_ant_total,
                'gestion' => $gestion,
                'mes' => $mes,
            ]);
            return $pdf->stream();
        }
        session()->flash('mensaje','Usuario no encontrado. Verifique e intente nuevamente.');
        return redirect()->route('marcados.reportes');   
    }

    public function semanal(){
        $datos=request() ->validate([
            'fecha_semana' => 'required'
        ]);
        $fecha_inicial = $datos['fecha_semana'];
        $fecha_actual = '';

        $usuarios = User::where('tipo','!=','administrador')->get();
        $arrayDatos = array();
        $dias_semana=['Monday'=>'Lunes','Tuesday'=>'Martes','Wednesday'=>'Miércoles','Thursday'=>'Jueves','Friday'=>'Viernes','Saturday'=>'Sábado','Sunday'=>'Domingo']; 
        foreach ($usuarios as $usuario){
            $dia = Carbon::createFromFormat('Y-m-d', $datos['fecha_semana']);
            $nom_dia = $dia->format('l');

            $arrayMes = array();            

            for ($i=1; $i <= 5 ; $i++) { 
                //$nom_dia = Carbon::createFromDate($gestion,$mes,$i)->format('l');

                //SABADOS Y DOMINGOS
                $fecha_actual = $dia->format('Y-m-d');
                if($nom_dia == 'Saturday' or $nom_dia == 'Sunday'){
                    $arrayMes[] = array(
                        'dia' => $fecha_actual,
                        'nombre_dia' => $dias_semana[$nom_dia],
                        'ingreso_m' => '', 'salida_m' => '', 'ingreso_t' => '', 'salida_t' => '', 'min_atraso' => '', 'salida_ant' => '', 'obs_entrada' => '', 'obs_salida' => '','horario_continuo'=>''
                    );
                    $dia = $dia->addDay();
                    $nom_dia = $dia->format('l');
                    continue;
                }
                //$fecha_actual = $gestion.'-'.$mes.'-'.$i;
                $marcados_dia = Marcado::where('usuario_id',$usuario->id)->where('fecha',$fecha_actual)->get();
                $ingreso_m = '';$salida_m = '';$ingreso_t = '';$salida_t = '';
                $time1 = Carbon::createFromFormat('H:i:s','09:30:00');
                $time2 = Carbon::createFromFormat('H:i:s','11:00:00');
                $time3 = Carbon::createFromFormat('H:i:s','13:59:59');
                $time4 = Carbon::createFromFormat('H:i:s','14:00:00');
                $time5 = Carbon::createFromFormat('H:i:s','15:30:00');
                $time6 = Carbon::createFromFormat('H:i:s','18:00:00');
                $time7 = Carbon::createFromFormat('H:i:s','23:59:00');
                $obs1=0;$obs2=0;$obs3=0;$obs4=0;
                foreach ($marcados_dia as $m_dia) {   // 4 registros
                    $time = Carbon::createFromFormat('H:i:s',$m_dia->hora);
                    if($time < $time1){
                        if($m_dia->comision == 'si'){
                            $ingreso_m = 'COMISION';
                        }else{
                            $ingreso_m = $m_dia->hora;
                        }
                        $obs1++;
                    }
                    if(($time > $time2) and ($time < $time3) ){
                        if($m_dia->comision == 'si'){
                            $salida_m = 'COMISION';
                        }else{
                            $salida_m = $m_dia->hora;
                        }
                        $obs2++;
                    }
                    if(($time > $time4) and ($time < $time5) ){
                        if($m_dia->comision == 'si'){
                            $ingreso_t = 'COMISION';
                        }else{
                            $ingreso_t = $m_dia->hora;
                        }
                        $obs3++;
                    }
                    if(($time > $time6) and ($time < $time7) ){
                        if($m_dia->comision == 'si'){
                            $salida_t = 'COMISION';
                        }else{
                            $salida_t = $m_dia->hora;
                        }
                        $obs4++;
                    }
                }
                $obs_entrada = 0; $obs_salida = 0;
                if($obs1 == 0){
                    $ingreso_m = '---';
                    $obs_entrada++;
                }
                if($obs2 == 0){
                    $salida_m = '---';
                    $obs_salida++;
                }
                if($obs3 == 0){
                    $ingreso_t = '---';
                    $obs_entrada++;
                }
                if($obs4 == 0){
                    $salida_t = '---';
                    $obs_salida++;
                }
                if(! Dia_Especial::where('fecha',$fecha_actual)->exists()){
                    $time1 = Carbon::createFromFormat('H:i:s','09:30:00');
                    $time2 = Carbon::createFromFormat('H:i:s','11:00:00');
                    $time3 = Carbon::createFromFormat('H:i:s','13:59:59');
                    $time4 = Carbon::createFromFormat('H:i:s','14:00:00');
                    $time5 = Carbon::createFromFormat('H:i:s','15:30:00');
                    $time6 = Carbon::createFromFormat('H:i:s','18:00:00');
                    $time7 = Carbon::createFromFormat('H:i:s','23:59:00');
                    $obs1=0;$obs2=0;$obs3=0;$obs4=0;
                    foreach ($marcados_dia as $m_dia) {   // 4 registros
                        $time = Carbon::createFromFormat('H:i:s',$m_dia->hora);
                        if($time < $time1){
                            if($m_dia->comision == 'si'){
                                $ingreso_m = 'COMISION';
                            }else{
                                $ingreso_m = $m_dia->hora;
                            }
                            $obs1++;
                        }
                        if(($time > $time2) and ($time < $time3) ){
                            if($m_dia->comision == 'si'){
                                $salida_m = 'COMISION';
                            }else{
                                $salida_m = $m_dia->hora;
                            }
                            $obs2++;
                        }
                        if(($time > $time4) and ($time < $time5) ){
                            if($m_dia->comision == 'si'){
                                $ingreso_t = 'COMISION';
                            }else{
                                $ingreso_t = $m_dia->hora;
                            }
                            $obs3++;
                        }

                        if(($time > $time6) and ($time < $time7) ){
                            if($m_dia->comision == 'si'){
                                $salida_t = 'COMISION';
                            }else{
                                $salida_t = $m_dia->hora;
                            }
                            $obs4++;
                        }
                    }

                    $obs_entrada = 0; $obs_salida = 0;
                    if($obs1 == 0){
                        $ingreso_m = '---';
                        $obs_entrada++;
                    }
                    if($obs2 == 0){
                        $salida_m = '---';
                        $obs_salida++;
                    }
                    if($obs3 == 0){
                        $ingreso_t = '---';
                        $obs_entrada++;
                    }
                    if($obs4 == 0){
                        $salida_t = '---';
                        $obs_salida++;
                    }
                    $continuo = 'no';
                }else{
                    /////////////HORARIO CONTINUO
                    $time1 = Carbon::createFromFormat('H:i:s','09:30:00');
                    $time2 = Carbon::createFromFormat('H:i:s','11:00:00');
                    $time5 = Carbon::createFromFormat('H:i:s','14:00:00');
                    $time6 = Carbon::createFromFormat('H:i:s','16:00:00');
                    $time7 = Carbon::createFromFormat('H:i:s','23:59:00');
                    $obs1=0;$obs2=0;$obs3=0;$obs4=0;
                    $c=0;
                    foreach ($marcados_dia as $m_dia) {   // 4 registros
                        $time = Carbon::createFromFormat('H:i:s',$m_dia->hora);
                        if($time < $time1){
                            if($m_dia->comision == 'si'){
                                $ingreso_m = 'COMISION';
                            }else{
                                $ingreso_m = $m_dia->hora;
                            }
                            $obs1++;
                        }

                        if(($time > $time2) and ($time < $time5) ){
                            
                            if($c==0){
                                $c++;
                                if($m_dia->comision == 'si'){
                                    $salida_m = 'COMISION';
                                }else{
                                    $salida_m = $m_dia->hora;
                                }
                                $obs2++;
                            }else{
                                if($m_dia->comision == 'si'){
                                    $ingreso_t = 'COMISION';
                                }else{
                                    $ingreso_t = $m_dia->hora;
                                }
                                $obs3++;
                            }
                        }

                        if(($time > $time6) and ($time < $time7) ){
                            if($m_dia->comision == 'si'){
                                $salida_t = 'COMISION';
                            }else{
                                $salida_t = $m_dia->hora;
                            }
                            $obs4++;
                        }
                    }

                    $obs_entrada = 0; $obs_salida = 0;
                    if($obs1 == 0){
                        $ingreso_m = '---';
                        $obs_entrada++;
                    }
                    if($obs2 == 0){
                        $salida_m = '---';
                        $obs_salida++;
                    }
                    if($obs3 == 0){
                        $ingreso_t = '---';
                        $obs_entrada++;
                    }
                    if($obs4 == 0){
                        $salida_t = '---';
                        $obs_salida++;
                    }
                    $continuo = 'si';
                }
                $arrayMes[] = array(
                    'dia' => $fecha_actual,
                    'nombre_dia' => $dias_semana[$nom_dia],
                    'ingreso_m' => $ingreso_m,
                    'salida_m' => $salida_m,
                    'ingreso_t' => $ingreso_t,
                    'salida_t' => $salida_t,
                    'obs_entrada' => $obs_entrada,
                    'obs_salida' => $obs_salida,
                    'horario_continuo' => $continuo,
                );
                $dia = $dia->addDay();
                $nom_dia = $dia->format('l');

            }
            $arrayDatos[] = array(
                'nom_usuario' => $usuario->ap_paterno.' '.$usuario->ap_materno.' '.$usuario->nombre,
                'arrayMes' => $arrayMes,
            );
        }
        return view('marcados.semanal',compact('arrayDatos','fecha_inicial','fecha_actual'));
    }




    public function semanalpdf(){
        $datos=request() ->validate([
            'fecha_semana' => 'required'
        ]);
        $fecha_inicial = $datos['fecha_semana'];
        $fecha_actual = '';
        $usuarios = User::where('tipo','!=','administrador')->get();
        $arrayDatos = array();
        $dias_semana=['Monday'=>'Lunes','Tuesday'=>'Martes','Wednesday'=>'Miércoles','Thursday'=>'Jueves','Friday'=>'Viernes','Saturday'=>'Sábado','Sunday'=>'Domingo']; 
        foreach ($usuarios as $usuario){
            $dia = Carbon::createFromFormat('Y-m-d', $datos['fecha_semana']);
            $nom_dia = $dia->format('l');
            $arrayMes = array();            
            for ($i=1; $i <= 5 ; $i++) { 
                //$nom_dia = Carbon::createFromDate($gestion,$mes,$i)->format('l');
                //SABADOS Y DOMINGOS
                $fecha_actual = $dia->format('Y-m-d');
                if($nom_dia == 'Saturday' or $nom_dia == 'Sunday'){
                    $arrayMes[] = array(
                        'dia' => $fecha_actual,
                        'nombre_dia' => $dias_semana[$nom_dia],
                        'ingreso_m' => '', 'salida_m' => '', 'ingreso_t' => '', 'salida_t' => '', 'min_atraso' => '', 'salida_ant' => '', 'obs_entrada' => '', 'obs_salida' => '','horario_continuo'=>''
                    );
                    $dia = $dia->addDay();
                    $nom_dia = $dia->format('l');
                    continue;
                }
                //$fecha_actual = $gestion.'-'.$mes.'-'.$i;
                $marcados_dia = Marcado::where('usuario_id',$usuario->id)->where('fecha',$fecha_actual)->get();
                $ingreso_m = '';$salida_m = '';$ingreso_t = '';$salida_t = '';
                $time1 = Carbon::createFromFormat('H:i:s','09:30:00');
                $time2 = Carbon::createFromFormat('H:i:s','11:00:00');
                $time3 = Carbon::createFromFormat('H:i:s','13:59:59');
                $time4 = Carbon::createFromFormat('H:i:s','14:00:00');
                $time5 = Carbon::createFromFormat('H:i:s','15:30:00');
                $time6 = Carbon::createFromFormat('H:i:s','18:00:00');
                $time7 = Carbon::createFromFormat('H:i:s','23:59:00');
                $obs1=0;$obs2=0;$obs3=0;$obs4=0;
                foreach ($marcados_dia as $m_dia) {   // 4 registros
                    $time = Carbon::createFromFormat('H:i:s',$m_dia->hora);
                    if($time < $time1){
                        if($m_dia->comision == 'si'){
                            $ingreso_m = 'COMISION';
                        }else{
                            $ingreso_m = $m_dia->hora;
                        }
                        $obs1++;
                    }
                    if(($time > $time2) and ($time < $time3) ){
                        if($m_dia->comision == 'si'){
                            $salida_m = 'COMISION';
                        }else{
                            $salida_m = $m_dia->hora;
                        }
                        $obs2++;
                    }
                    if(($time > $time4) and ($time < $time5) ){
                        if($m_dia->comision == 'si'){
                            $ingreso_t = 'COMISION';
                        }else{
                            $ingreso_t = $m_dia->hora;
                        }
                        $obs3++;
                    }
                    if(($time > $time6) and ($time < $time7) ){
                        if($m_dia->comision == 'si'){
                            $salida_t = 'COMISION';
                        }else{
                            $salida_t = $m_dia->hora;
                        }
                        $obs4++;
                    }
                }
                $obs_entrada = 0; $obs_salida = 0;
                if($obs1 == 0){
                    $ingreso_m = '---';
                    $obs_entrada++;
                }
                if($obs2 == 0){
                    $salida_m = '---';
                    $obs_salida++;
                }
                if($obs3 == 0){
                    $ingreso_t = '---';
                    $obs_entrada++;
                }
                if($obs4 == 0){
                    $salida_t = '---';
                    $obs_salida++;
                }
                if(! Dia_Especial::where('fecha',$fecha_actual)->exists()){
                    $time1 = Carbon::createFromFormat('H:i:s','09:30:00');
                    $time2 = Carbon::createFromFormat('H:i:s','11:00:00');
                    $time3 = Carbon::createFromFormat('H:i:s','13:59:59');
                    $time4 = Carbon::createFromFormat('H:i:s','14:00:00');
                    $time5 = Carbon::createFromFormat('H:i:s','15:30:00');
                    $time6 = Carbon::createFromFormat('H:i:s','18:00:00');
                    $time7 = Carbon::createFromFormat('H:i:s','23:59:00');
                    $obs1=0;$obs2=0;$obs3=0;$obs4=0;
                    foreach ($marcados_dia as $m_dia) {   // 4 registros
                        $time = Carbon::createFromFormat('H:i:s',$m_dia->hora);
                        if($time < $time1){
                            if($m_dia->comision == 'si'){
                                $ingreso_m = 'COMISION';
                            }else{
                                $ingreso_m = $m_dia->hora;
                            }
                            $obs1++;
                        }
                        if(($time > $time2) and ($time < $time3) ){
                            if($m_dia->comision == 'si'){
                                $salida_m = 'COMISION';
                            }else{
                                $salida_m = $m_dia->hora;
                            }
                            $obs2++;
                        }
                        if(($time > $time4) and ($time < $time5) ){
                            if($m_dia->comision == 'si'){
                                $ingreso_t = 'COMISION';
                            }else{
                                $ingreso_t = $m_dia->hora;
                            }
                            $obs3++;
                        }

                        if(($time > $time6) and ($time < $time7) ){
                            if($m_dia->comision == 'si'){
                                $salida_t = 'COMISION';
                            }else{
                                $salida_t = $m_dia->hora;
                            }
                            $obs4++;
                        }
                    }

                    $obs_entrada = 0; $obs_salida = 0;
                    if($obs1 == 0){
                        $ingreso_m = '---';
                        $obs_entrada++;
                    }
                    if($obs2 == 0){
                        $salida_m = '---';
                        $obs_salida++;
                    }
                    if($obs3 == 0){
                        $ingreso_t = '---';
                        $obs_entrada++;
                    }
                    if($obs4 == 0){
                        $salida_t = '---';
                        $obs_salida++;
                    }
                    $continuo = 'no';
                }else{
                    /////////////HORARIO CONTINUO
                    $time1 = Carbon::createFromFormat('H:i:s','09:30:00');
                    $time2 = Carbon::createFromFormat('H:i:s','11:00:00');
                    $time5 = Carbon::createFromFormat('H:i:s','14:00:00');
                    $time6 = Carbon::createFromFormat('H:i:s','16:00:00');
                    $time7 = Carbon::createFromFormat('H:i:s','23:59:00');
                    $obs1=0;$obs2=0;$obs3=0;$obs4=0;
                    $c=0;
                    foreach ($marcados_dia as $m_dia) {   // 4 registros
                        $time = Carbon::createFromFormat('H:i:s',$m_dia->hora);
                        if($time < $time1){
                            if($m_dia->comision == 'si'){
                                $ingreso_m = 'COMISION';
                            }else{
                                $ingreso_m = $m_dia->hora;
                            }
                            $obs1++;
                        }

                        if(($time > $time2) and ($time < $time5) ){
                            
                            if($c==0){
                                $c++;
                                if($m_dia->comision == 'si'){
                                    $salida_m = 'COMISION';
                                }else{
                                    $salida_m = $m_dia->hora;
                                }
                                $obs2++;
                            }else{
                                if($m_dia->comision == 'si'){
                                    $ingreso_t = 'COMISION';
                                }else{
                                    $ingreso_t = $m_dia->hora;
                                }
                                $obs3++;
                            }
                        }

                        if(($time > $time6) and ($time < $time7) ){
                            if($m_dia->comision == 'si'){
                                $salida_t = 'COMISION';
                            }else{
                                $salida_t = $m_dia->hora;
                            }
                            $obs4++;
                        }
                    }

                    $obs_entrada = 0; $obs_salida = 0;
                    if($obs1 == 0){
                        $ingreso_m = '---';
                        $obs_entrada++;
                    }
                    if($obs2 == 0){
                        $salida_m = '---';
                        $obs_salida++;
                    }
                    if($obs3 == 0){
                        $ingreso_t = '---';
                        $obs_entrada++;
                    }
                    if($obs4 == 0){
                        $salida_t = '---';
                        $obs_salida++;
                    }
                    $continuo = 'si';
                }
                $arrayMes[] = array(
                    'dia' => $fecha_actual,
                    'nombre_dia' => $dias_semana[$nom_dia],
                    'ingreso_m' => $ingreso_m,
                    'salida_m' => $salida_m,
                    'ingreso_t' => $ingreso_t,
                    'salida_t' => $salida_t,
                    'obs_entrada' => $obs_entrada,
                    'obs_salida' => $obs_salida,
                    'horario_continuo' => $continuo,
                );
                $dia = $dia->addDay();
                $nom_dia = $dia->format('l');
            }
            $arrayDatos[] = array(
                'nom_usuario' => $usuario->ap_paterno.' '.$usuario->ap_materno.' '.$usuario->nombre,
                'arrayMes' => $arrayMes,
            );
        }
        $pdf = \PDF::loadview('marcados.semana_pdf',[
            'arrayDatos'=>$arrayDatos,
            'fecha_inicial' => $fecha_inicial,
            'fecha_actual' => $fecha_actual,
        ]);
        return $pdf->stream();
    }

}