<?php

namespace App\Http\Controllers;

use App\User;
//use App\Suscripcion;
use Illuminate\Http\Request;


class UserController extends Controller
{



	public function index(Request $request){

    	/*if (Gate::denies('es_admin_gestor')) {
            $error = 'accion no autorizada';
            return redirect()->route('eventos.index');
        }*/

    	//$ci = $request['ci'];
        //$tipo = $request['tipo'];  	

        /*if(isset($ci)){
        	if(is_numeric($ci)) {
        		$usuarios =User::orderBy('ap_paterno','ASC')
		        ->ci($ci)
		        ->tipo($tipo)
		        ->paginate(50);
        	}else{
				$usuarios =User::orderBy('ap_paterno','ASC')
		        ->nombre($ci)
		        ->tipo($tipo)
		        ->paginate(50);
        	}
        }
	  	else{
			$usuarios =User::orderBy('ap_paterno','ASC')
	        ->ci($ci)
	        ->tipo($tipo)
	        ->paginate(50);
	  	}*/
	  	$usuarios=User::all();
	  	//dd('ddddddd');
    	return view('usuarios.index', compact('usuarios'));
    }

    public function show(User $usuario){
		//$usuario=Usuario::findOrFail($id);

		/*if (Gate::denies('es_admin_gestor')) {
            $verificar=$usuario;
        	$this->authorize('autorizar_mi_usuario', $verificar);
        }*/
	
		return view('usuarios.show',compact('usuario'));
	}
	
	public function create(){
		/*if (Gate::denies('es_admin_gestor')) {
            $error = 'accion no autorizada';
            return redirect()->route('eventos.index');
        }*/

		return view('usuarios.create');
	}
	
	public function store(){
		/*if (Gate::denies('es_admin_gestor')) {
            $error = 'accion no autorizada';
            return redirect()->route('eventos.index');
        }*/


		$datos=request() ->validate([
			'ci' => ['required','numeric','unique:usuarios,ci'],
			'tipo' => '',
			'ap_paterno' => '',
			'ap_materno' => '',
			'nombre' => 'required',
			'email' => ['required','email','unique:usuarios,email'],
			'password' => 'required|confirmed',
		],[
			'nombre.required' => 'El campo nombre es obligatorio'
		]);



		$user = User::create([
			'ci' => $datos['ci'],
			'tipo' => $datos['tipo'],
			'nombre' => $datos['nombre'],
			'ap_paterno' => $datos['ap_paterno'],
			'ap_materno' => $datos['ap_materno'],
			'email' => $datos['email'],
			'password' => bcrypt($datos['password']),
		]);
		//$correo = $user->notificar_registro(); //envia correo notificando el registro al sistema
		session()->flash('mensaje','Usuario creado correctamente');

		return redirect()->route('usuarios.index');
	}

	public function edit(User $usuario){

		return view('usuarios.edit', ['usuario' => $usuario]);
	}

	public function update(User $usuario){

		$datos= request()->validate([
			'ci' => 'required|numeric|unique:usuarios,ci,'.$usuario->id, //nos excluimos de comprobar 
			'tipo' => 'required',
			'ap_paterno' => '',
			'ap_materno' => '',
			'nombre' => 'required',
			
			'email' => 'required|email|unique:usuarios,email,'.$usuario->id, //nos excluimos de comprobar q sea email unico
			
			'password' => 'confirmed',
		]);
		$datos['tipo'] = $datos['tipo'];

		if($datos['password'] !=null){
            $datos['password'] = bcrypt($datos['password']);
        }else{
            unset($datos['password']); //quitar de $datos
        }

		$usuario->update($datos);
		session()->flash('mensaje','Usuario modificado correctamente');

		return redirect()->route('usuarios.show', ['usuario' => $usuario]);
	}

	
	
	public function destroy(User $usuario){

		

		$usuario->delete();

		session()->flash('mensaje','Usuario borrado');
		
		return redirect()->route('usuarios.index');
	}

}
