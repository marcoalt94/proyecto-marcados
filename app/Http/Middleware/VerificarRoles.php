<?php

namespace App\Http\Middleware;

use Closure;

class VerificarRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $rol)
    {
        //verifica que el usuario tenga el rol de "administrador"
        $tipo = $request->user()->tipo;  

        if (!($tipo == $rol) and ($tipo != 'administrador')  ) {
            abort(403, 'Unauthorized action.');
            return redirect('/');
        }

        return $next($request);
    }
}
