<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dia_Especial extends Model
{
	protected $table='dias_especiales';

    protected $fillable = [

        'fecha','tipo'
    ];


    public function empleado(){
        return $this-> belongsTo(User::class);
    }

}
