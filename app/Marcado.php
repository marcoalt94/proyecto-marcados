<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Marcado extends Model
{

    protected $fillable = [

        'usuario_id','fecha','hora','comision'
    ];


    public function empleado(){
        return $this-> belongsTo(User::class);
    }

}
