$(document).ready(function(){

var tipo = $('#tipoPregunta').val();
if(tipo == 'abierta'){
	$('#opciones').hide();
	$('#seleccionMultiple').hide();
}else{
	if(tipo == 'opcional'){
		$('#opciones').show();
	    $('#seleccionMultiple').hide();
	}else{
		$('#seleccionMultiple').show();
	    $('#opciones').hide();
	}
}


$('#tipoPregunta').change(function(){
	var tipo = $('#tipoPregunta').val();
	if(tipo == 'abierta'){
		$('#opciones').hide();
		$('#seleccionMultiple').hide();
	}else{
		if(tipo == 'opcional'){
			$('#nuevaOpcion').val("");
			$('#opciones').show();
		    $('#seleccionMultiple').hide();
		}else{
			$('#seleccionMultiple').show();
		    $('#opciones').hide();
		}
	}
});



});


var contador = 0;
/*agrega una nueva opcion a la vista con su respectivo nombre y valor*/
function agregarOpcion(){
	contador += 1;
	var nombreOpcion = $('#nuevaOpcion').val(); 
	if(nombreOpcion.length > 0){
		
		$('#seccionOpciones').append('<i class="far fa-dot-circle"></i><input type="text" class="ml-2 mt-2 text-center" name="opcion_'+contador+'" value="'+nombreOpcion+'" readonly><br>');	
	
	}
	$('#nuevaOpcion').val("");
};

var contador2=0;
/*agrega una nueva opcion a la vista con su respectivo nombre y valor, a una pregunta de tipo seleccion multiple*/
function agregarSeleccion(){
	contador2 += 1;
	var nombreSeleccion = $('#nuevaSeleccion').val();
	if(nombreSeleccion.length > 0){
		/*// $('#seccionSeleccion').append('<tr><td><i class="far fa-square"></i></td><td><input type="text" class="form-control mt-2 ml-2" name="opcion_'+contador2+'" value="'+nombreSeleccion+'"></td></tr>');	
		*/
		$('#seccionSeleccion').append('<i class="far fa-square"></i><input type="text" class="ml-2 mt-2 text-center" name="seleccion_'+contador2+'" value="'+nombreSeleccion+'" readonly><br>');	
	}
	$('#nuevaSeleccion').val("");

};

//anula el submit a un formulario por medio de la tecla Enter
function anular(e) {
	tecla = (document.all) ? e.keyCode : e.which;
	return (tecla != 13);
}