
function registrarAsistencia(usuario){
    /*Esta funcion es llamada desde la pagina de registro de asistencia, se utiliza para registrar
    la asistencia de usuarios desde el listado de usuarios inscritos al evento*/

	var evento = $("#evento_id").val();
    var route = "https://eventos.fcpn.edu.bo/eventos/asistenciaAjax"
    var token = $("#token").val();
   
    $.ajax({
        url:route,
        headers: {'X-CSRF-TOKEN': token},
        type:'POST',
        dataType:'json',
        data:{
            evento_id : evento,
            usuario_id : usuario       
        }
    });
    $("#asistenciaUsuario"+usuario).hide();
    $("#exito"+usuario).text('Asistencia registrada');
};


