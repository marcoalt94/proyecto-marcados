/* abre una nueva ventana para compartir el sitio en redes sociales*/
function genericSocialShare(url){
    window.open(url,'sharer','toolbar=0,status=0,width=648,height=395');
    return true;
}
//abre un modal con informacion requerida
function openModal(){
	event.preventDefault()
	var ci = $("#ci_participante").val();

	if(ci!=''){
		$('#ciParticipante').val(ci);
    	$('#modalInscribirme').modal();
	}

}

//requerido por bootstrap
$(function(){
  $('[data-toggle="popover"]').popover()
});

//en la crearcion o edicion de eventos ayuda describir las caracteristicas del evento mostrando u ocultando
//distintos bloques de código en las vistas
if($('input:radio[name=inscribir]:checked').val() == "si"){
	$('#opcion_inscribir').show();
}
if($('input:radio[name=inscribir]:checked').val() == "no"){
	$('#opcion_inscribir').hide();
	$('#cupos').val(0);
}
function si_inscribir(){
	$('#opcion_inscribir').show();
};
function no_inscribir(){
	$('#opcion_inscribir').hide();
	$('#cupos').val(0);
	$('input:radio[name=registrar_pago]:nth(0)').prop('checked',true);
	
};


//requerido por bootstrap
$(function(){
  $('[data-toggle="tooltip"]').tooltip();
});

function busqueda_opcion(){
	$("#busquedaAvanzada").toggleClass("d-none");
};


// ayuda en la creacion de contraseñas seguras
$(function(){
	var len = new RegExp("^(?=.{8,})");
	var mayus = new RegExp("^(?=.*[A-Z])");
	var lower = new RegExp("^(?=.*[a-z])");
	var numbers = new RegExp("^(?=.*[0-9])");
	var regExp = [len, mayus,lower,numbers];
	var elementos = [$("#longitud"),$("#mayuscula"),$("#minuscula"),$("#digito")];
	$("#password").on("keyup", function(){
		var pass= $("#password").val();
		for (var i = 0; i <4; i++) {
			if (regExp[i].test(pass)) {
				elementos[i].hide();
			}else{
				$("#password").addClass("is-invalid");
				elementos[i].show();
			}
		}
		if(len.test(pass) && mayus.test(pass) && lower.test(pass) && numbers.test(pass)){
			$("#password").removeClass("is-invalid");
			$("#mensaje").text("Contraseña segura").css("color","green");
		}else{
			$("#mensaje").text("Contraseña insegura").css("color","red");
		}
	});
	//ayuda a comprobar que se introduzca la misma contraseña en las partes requeridas
	$("#password-confirm").on("keyup", function(){
		var pass= $("#password").val();
		var confirm= $("#password-confirm").val();
		if(pass != confirm){
			$("#password-confirm").addClass("is-invalid");
		}else{
			$("#password-confirm").removeClass("is-invalid");
			if(len.test(pass) && mayus.test(pass) && lower.test(pass) && numbers.test(pass)){
				$("#botonAceptar").prop('disabled', false);
			}
		}
	});
	$("#password-confirm2").on("keyup", function(){
		var pass= $("#password2").val();
		var confirm= $("#password-confirm2").val();
		if(pass != confirm){
			$("#password-confirm2").addClass("is-invalid");
		}else{
			$("#password-confirm2").removeClass("is-invalid");
		}
	});
});

//agrega animaciones para algunos componentes del sistema
$(document).ready(function(){
	$("#tituloAnimado").addClass('animated zoomIn');

	$("#seccionAfiche").addClass('animated fadeInLeft');

	$("#seccionDescripcion").addClass('animated slideInUp delay-1s');

	$("#suscripcionAnimacion").addClass('animated slideInUp');

	$("#botonAnimado").addClass('animated tada delay-2s');

	$("#portadaAnimado").addClass('animated bounceIn');
});

//para agregar varios afiches
$(document).ready(function(){
	$('#crearAfiche2').hide();
	$('#crearAfiche3').hide();
	$('#crearAfiche4').hide();
});

function nuevo_afiche2(){
	$('#crearAfiche2').show();	
};

function nuevo_afiche3(){
	$('#crearAfiche3').show();
};

function nuevo_afiche4(){
	$('#crearAfiche4').show();
};

function ocultar_afiche2(){
	$('#crearAfiche2').hide();
	$('#crearAfiche3').hide();
	$('#crearAfiche4').hide();
	$('#cargarImagen2').val('');
	$('#cargarImagen3').val('');
	$('#cargarImagen4').val('');
};

function ocultar_afiche3(){
	$('#crearAfiche3').hide();
	$('#crearAfiche4').hide();
	$('#cargarImagen3').val('');
	$('#cargarImagen4').val('');
};

function ocultar_afiche4(){
	$('#crearAfiche4').hide();

	$('#cargarImagen4').val('');
};


//ayuda a crear un nuevo tipo de evento
var selectTipo = $('#tipo option:selected').val();
if(selectTipo != 'otro'){
	$('#otroTipo').hide();
}else{
	$('#otroTipo').show();
}

//ayuda a crear un nuevo lugar
var selectLugar = $('#lugar option:selected').val();
if(selectLugar != 'otro'){
	$('#otroLugar').hide();
	$('#otroCoordenadas').hide();
}else{
	$('#otroLugar').show();
	$('#otroCoordenadas').show();
}

$('select[name=tipo]').change(function(){
    var seleccionadoTipo = $('select[name=tipo]').val();
    if(seleccionadoTipo == 'otro'){
    	$('#otroTipo').show();
    }else{
    	$('#otroTipo').hide();
    }
});
$('select[name=lugar]').change(function(){
    var seleccionadoLugar = $('select[name=lugar]').val();
    if(seleccionadoLugar == 'otro'){
    	$('#otroLugar').show();
    	$('#otroCoordenadas').show();
    }else{
    	$('#otroLugar').hide();
    	$('#otroCoordenadas').hide();
    }
});

//habilita un campo para crear carreras o unidades 
var selectCarrera = $('#carrera option:selected').val();
if(selectCarrera != 'otro'){
	$('#otraUnidad').hide();
}
var selectUnidad = $('#unidades_carrera option:selected').val();
if(selectUnidad != 'otro'){
	$('#otraUnidadCarrera').hide();
}


