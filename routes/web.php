<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

Route::get('/home'//, 'HomeController@index')->name('home');
, function () {
	    //return view('auth.login');
	    return redirect()->route('marcados.index');
	});

Route::get('/proyMarcados/public', function () {
    return redirect()->route('marcados.index');
});

Route::middleware(['auth'])->group(function(){


	Route::get('/', function () {
	    //return view('auth.login');
	    return redirect()->route('marcados.index');
	});

	Route::get('/usuarios', 'UserController@index')->name('usuarios.index')->middleware('rol:administrador');
	Route::get('/usuarios/{usuario}', 'UserController@show')->name('usuarios.show')->middleware('rol:administrador');
	Route::get('/usuario/nuevo', 'UserController@create')->name('usuarios.create')->middleware('rol:administrador');
	Route::post('/usuarios', 'UserController@store')->middleware('rol:administrador');
	Route::get('/usuarios/{usuario}/editar', 'UserController@edit')->name('usuarios.edit')->middleware('rol:administrador');
	Route::put('usuarios/{usuario}', 'UserController@update')->name('usuarios.update')->middleware('rol:administrador');
	Route::delete('/usuarios/{usuario}', 'UserController@destroy')->name('usuarios.destroy')->middleware('rol:administrador');
	
	//pagina inicial
	Route::get('/marcados', 'MarcadoController@index')->name('marcados.index');

	//formulario
	Route::get('/marcados/reportes', 'MarcadoController@reportes')->name('marcados.reportes')->middleware('rol:administrador');

	//buscar reporte
	//Route::post('/marcados/buscar', 'MarcadoController@buscar')->name('marcados.buscar');	
	//Route::get('/marcados/detalle', 'MarcadoController@detalle')->name('marcados.detalle')->middleware('rol:administrador');
	Route::get('/marcados/detalle/{ci}/{mes}/{gestion}', 'MarcadoController@detalle')->name('marcados.detalle')->middleware('rol:administrador');
	Route::get('/marcados/detallepdf/{ci}/{mes}/{gestion}', 'MarcadoController@detallepdf')->name('marcados.mes_pdf')->middleware('rol:administrador');
	

	Route::get('/marcados/comision','MarcadoController@comision')->name('marcados.comision')->middleware('rol:administrador');
	Route::post('/marcados/comision', 'MarcadoController@asignarComision')->middleware('rol:administrador');


	Route::get('/marcados/especial','MarcadoController@dia_especial')->name('marcados.especial')->middleware('rol:administrador');
	Route::post('/marcados/especial', 'MarcadoController@registro_especial')->middleware('rol:administrador');


	Route::post('/marcados/reporte_semanal', 'MarcadoController@semanal')->name('marcados.semanal')->middleware('rol:administrador');
	Route::post('/marcados/semanal_pdf', 'MarcadoController@semanalpdf')->name('marcados.semanalpdf')->middleware('rol:administrador');
	

});






Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
